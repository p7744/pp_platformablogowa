from django.shortcuts import render
from django.http import HttpResponseBadRequest
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser
from django.core.paginator import Paginator

from .models import *
from .serializers import *
from .permissions import *

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from .swagger_schema import *

# TODO - permissions and checking of users permissions

DEFAULT_PER_PAGE = 10


class ArticleView(viewsets.ViewSet):
    
    permission_classes = [IsGetOrIsAuthenticated,]
    parser_classes = (MultiPartParser,)
    
    @swagger_auto_schema(
        name="Get articles",
        operation_description="Get articles",
        manual_parameters=[swagger_schema_params['page'], 
                           swagger_schema_params['per_page'], 
                           swagger_schema_params['author'],
                           swagger_schema_params['category']],
        operation_id="Get All articles",
        responses={200: ArticleSerializer(many=True), 400: "Bad Request", 404: "Not Found"}
    )
    def get_article(self, request):
        articles = Article.objects.filter(isActive = True).order_by("-createdAt")
        
        additional_queries = ""
        
        if a_category := request.GET.get('category'):
            articles = articles.filter(category = a_category)
            if not articles:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&category={a_category}" 
            
        if a_author := request.GET.get('author_id'):
            articles = articles.filter(authorID = a_author)
            if not articles:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&author_id={a_author}" 
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(articles, per_page=per_page)
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            articles = paginator.page(int(page))
            serializer = ArticleSerializer(articles, many = True)
            return Response({
                "count": len(articles),
                "next": (f"http://127.0.0.1:8001/api/v1/articles?page={int(page)+1}&per_page={per_page}" + additional_queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8001/api/v1/articles?page={int(page)-1}&per_page={per_page}" + additional_queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        serializer = ArticleSerializer(articles, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)

    @swagger_auto_schema(
        name="Create new article",
        operation_description="Create new article",
        operation_id="Create new article",
        request_body=CreateArticleSerializer,
        responses={201: ArticleSerializer, 400: "Bad Request"}
    )
    def create_article(self, request):
        '''
        Create new article
        '''
        data = request.data
        data._mutable = True
        data['authorID'] = request.userid
        data._mutable = False
        serializer = CreateArticleSerializer(data = data)
        if not serializer.is_valid():
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        
        val_data = serializer.validated_data
        val_data['authorID'] = request.userid
        article = serializer.create(serializer.validated_data)
        return Response(ArticleSerializer(article).data, status = status.HTTP_201_CREATED)
        

class ArticleDetailView(viewsets.ViewSet):
    
    permission_classes = [IsGetOrIsAuthenticated,]
    
    def get_detail_article(self, articleID):
        '''
        Returns article with given id or None
        '''
        try:
            return Article.objects.get(articleID = articleID)
        except Article.DoesNotExist:
            return None

    @swagger_auto_schema(
        operation_description="Get article by its id",
        operation_id="Get article by its id",
        responses={200: ArticleSerializer, 404: "Not Found"}
    )
    def get_article(self, request, articleID):
        '''
        Get article
        '''
        article = self.get_detail_article(articleID)
        if not article:
            return Response(status = status.HTTP_404_NOT_FOUND)
        serializer = ArticleSerializer(article)
        return Response(serializer.data , status = status.HTTP_200_OK)
    
    @swagger_auto_schema(
        operation_description="Modify article by its id",
        operation_id="Modify article of given id",
        request_body=EditArticleSerializer,
        responses={200: ArticleSerializer, 403: "Forbidden", 404: "Not Found"}
    )
    def modify_article(self, request, articleID):
    
        serializer = EditArticleSerializer(data=request.data)
        if serializer.is_valid():
            article = self.get_detail_article(articleID)
            if not article:
                return Response(status=status.HTTP_404_NOT_FOUND)
            if article.authorID != request.userid:
                return Response(status=status.HTTP_403_FORBIDDEN)
            article.title = serializer.validated_data.get('title')
            article.body = serializer.validated_data.get('body')
            article.category = serializer.validated_data.get('category')
            article.isActive = serializer.validated_data.get('isActive')
            article.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    @swagger_auto_schema(
        operation_description="Deactivate article by its id",
        operation_id="Delete article of given id",
        responses={204: "No Content", 403: "Forbidden", 404: "Not Found"}
    )
    def delete_article(self, request, articleID):
        '''
        Delete article
        '''
        article = self.get_detail_article(articleID)

        if article:
            if article.authorID != request.userid:
                return Response(status=status.HTTP_403_FORBIDDEN)
            article.isActive = False
            article.save()
            return Response(status = status.HTTP_204_NO_CONTENT)
        
        return Response(status.HTTP_404_NOT_FOUND)
        
class ReactionsView(viewsets.ViewSet):
    
    permission_classes = [IsGetOrIsAuthenticated,]
    
    @swagger_auto_schema(
        operation_description="Get reactions",
        operation_id="Get all reactions",
        manual_parameters=[
            swagger_schema_params['page'],
            swagger_schema_params['per_page'],
            swagger_schema_params['author'],
            swagger_schema_params['article'],
        ],
        responses={200: ReactionSerializer, 404: "Not Found"}
    )
    def get_reactions(self, request):
        reactions = Reaction.objects.all().order_by("-reactedAt")
        additional_queries = ""
        
        articleID = request.GET.get('article_id')
        if articleID:
            reactions = reactions.filter(article__articleID = articleID)
            if not reactions:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&article_id={articleID}"
            
        authorID = request.GET.get('author_id')
        if authorID:
            reactions = reactions.filter(authorID = authorID)
            if not reactions:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&author_id={authorID}"
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(reactions, per_page=int(per_page))
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            reactions = paginator.page(int(page))
            serializer = ReactionSerializer(reactions, many = True)
            return Response({
                "count": len(reactions),
                "next": (f"http://127.0.0.1:8001/api/v1/reactions?page={int(page)+1}&per_page={per_page}" + additional_queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8001/api/v1/reactions?page={int(page)-1}&per_page={per_page}" + additional_queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        serializer = ReactionSerializer(reactions, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)

    @swagger_auto_schema(
        tags=['reactions'],
        operation_id="Get a reaction by its id",
        operation_description="Get a reaction by its id",
        responses={200: ReactionSerializer, 404: "Not Found"}
    )
    def get_reaction_by_id(self, request, reactionID):
        try:
            reaction = Reaction.objects.get(reactionID = reactionID)
        except Reaction.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        return Response(ReactionSerializer(reaction).data, status=status.HTTP_200_OK)
        
    @swagger_auto_schema(
        tags=['reactions'],
        operation_description="Delete a reaction to the article",
        responses={204: "No Content", 400: "Bad_request", 404: "Not Found"}
    )
    def delete_reaction(self, request, reactionID):
        try:
            reactions: Reaction = Reaction.objects.get(reactionID = reactionID)
        except Reaction.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if reactions.authorID != request.userid:
            return Response({"Author": reactions.authorID, "User": request.user.pk},status=status.HTTP_403_FORBIDDEN)
        reactions.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        operation_description="Add new reaction to the article",
        request_body=ReactionSerializer,
        responses={201: ReactionSerializer, 400: "Bad Request"}
    )
    def add_reaction(self, request):
        data = request.data
        data['authorID'] = request.userid
        serializer = ReactionSerializer(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        reaction = serializer.create(serializer.validated_data)
        return Response(ReactionSerializer(reaction).data, status = status.HTTP_201_CREATED)

class CommentsView(viewsets.ViewSet):
    
    permission_classes = [IsGetOrIsAuthenticated,]
    
    @swagger_auto_schema(
        operation_description="Get comments",
        operation_id="Get all comments", 
        manual_parameters=[
            swagger_schema_params['page'],
            swagger_schema_params['per_page'],
            swagger_schema_params['author'],
            swagger_schema_params['article'],
        ],
        responses={200: CommentSerializer, 404: "Not Found"}
    )
    def get(self, request):
       
        comments = Comment.objects.all().order_by("-createdAt")
        additional_queries = ""
        
        articleID = request.GET.get('article_id')
        if articleID:
            comments = comments.filter(article__articleID=articleID)
            if not comments:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&article_id={articleID}"
            
        authorID = request.GET.get('author_id')
        if authorID:
            comments = comments.filter(authorID = authorID)
            if not comments:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&author_id={authorID}"
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(comments, per_page=int(per_page))
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            comments = paginator.page(int(page))
            serializer = CommentSerializer(comments, many = True)
            return Response({
                "count": len(comments),
                "next": (f"http://127.0.0.1:8001/api/v1/comments?page={int(page)+1}&per_page={per_page}" + additional_queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8001/api/v1/comments?page={int(page)-1}&per_page={per_page}" + additional_queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        serializer = CommentSerializer(comments, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)
        
    @swagger_auto_schema(
        operation_description="Get a comment to the article",
        operation_id="Get comment by its id",
        responses={200: CommentSerializer, 404: "Not Found"}
    )
    def get_one(self, request, commentID):
        comment = Comment.objects.filter(commentID = commentID).first()
        if not comment:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        return Response(CommentSerializer(comment).data,status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        operation_description="Add new comment to the article",
        request_body=CommentSerializer,
        responses={201: CommentSerializer, 400: "Bad Request"}
    )
    def post(self, request):
        data = request.data
        data['authorID'] = request.userid
        serializer = CommentSerializer(data = data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)
        comment = Comment(**serializer.validated_data)
        comment.save()
        return Response(CommentSerializer(comment).data, status = status.HTTP_201_CREATED)
    
    @swagger_auto_schema(
        operation_description="Modify comment to the article",
        request_body=EditCommentSerializer,
        responses={200: CommentSerializer, 404: "Not Found"}
    )
    def modify(self, request, commentID):
        comment = Comment.objects.filter(commentID = commentID).first()
        if not comment:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        if comment.authorID != request.userid:
            return Response(status=status.HTTP_403_FORBIDDEN)
        
        serializer = EditCommentSerializer(data=request.data)
        if serializer.is_valid():
            comment.description = serializer.validated_data.get('description')
            comment.save()
            return Response(CommentSerializer(comment).data,status=status.HTTP_200_OK)
        
        return Response(status=status.HTTP_400_BAD_REQUEST)
    
    @swagger_auto_schema(
        operation_description="Delete comment to the article",
        responses={204: "No Content", 404: "Not Fount"}
    )
    def delete(self, request, commentID):
        comment = Comment.objects.filter(commentID = commentID).first()
        if not comment:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        if comment.authorID != request.userid:
            return Response(status=status.HTTP_403_FORBIDDEN)
        
        comment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
