from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):
    class Meta:
        unique_together = (('articleID', 'authorID'),)
    
    articleID = models.BigAutoField(primary_key = True)
    authorID = models.IntegerField(null=False)
    title = models.TextField(null=False)
    body = models.TextField(default="")
    createdAt = models.DateTimeField(default=timezone.now)
    category = models.CharField(max_length = 50)
    isActive = models.BooleanField(default=False)
    
    def __str__(self):
        return self.title


class Reaction(models.Model):
    class Meta:
        unique_together = (('authorID', 'article'))
    
    reactionID = models.BigAutoField(primary_key=True)
    authorID = models.IntegerField(null=False)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    liked = models.BooleanField()
    reactedAt = models.DateTimeField(default=timezone.now)


class Comment(models.Model):

    commentID = models.BigAutoField(primary_key=True)
    authorID = models.IntegerField(null=False)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    description = models.TextField()
    createdAt = models.DateTimeField(default=timezone.now)

