from django.contrib import admin
from django.urls import path, include
from . import views
from rest_framework import routers

from .views import *


urlpatterns = [

    path('articles', views.ArticleView.as_view({'get': 'get_article', 'post': 'create_article'})),
    path('articles/<int:articleID>', views.ArticleDetailView.as_view({'get': 'get_article', 'delete': 'delete_article', 'put': "modify_article"})),
    
    path('comments', views.CommentsView.as_view({'get': 'get', 'post': 'post'})),
    path('comments/<int:commentID>', views.CommentsView.as_view({'delete': 'delete', 'put': 'modify'})),
    path('reactions', views.ReactionsView.as_view({'get': 'get_reactions', 'post': 'add_reaction'})),
    path('reactions/<int:reactionID>', views.ReactionsView.as_view({'get': 'get_reaction_by_id', 'delete': 'delete_reaction'})),
    #path('reactions/<int:authorID>/', views.CommentsView.as_view({'get': 'get_user_reaction', 'delete': 'delete_reaction'}))

]
