import re
from drf_yasg import openapi

swagger_schema_params = {
    'page': openapi.Parameter('page', openapi.IN_QUERY, description="Number of Page", type=openapi.TYPE_INTEGER),
    'per_page': openapi.Parameter('per_page', openapi.IN_QUERY, description="Number of profiles per page", type=openapi.TYPE_INTEGER),
    'author': openapi.Parameter('author_id', openapi.IN_QUERY, description="Id of an author", type=openapi.TYPE_INTEGER),
    'article': openapi.Parameter('article_id', openapi.IN_QUERY, description="Id of an article", type=openapi.TYPE_INTEGER),
    'category': openapi.Parameter('category', openapi.IN_QUERY, description="category name", type=openapi.TYPE_STRING),
    'comment_desc': openapi.Parameter('description', openapi.IN_BODY, description="New description of comment", type=openapi.TYPE_STRING, required=True)
}
