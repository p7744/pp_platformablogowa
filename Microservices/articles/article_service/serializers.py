from rest_framework import serializers
from .models import *


class ReactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reaction
        fields = ["reactionID", "authorID", "article", "liked", "reactedAt"]
        read_only_fields = ["reactionID", "reactedAt"]


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ["commentID", "authorID", "article", "description", "createdAt"]
        read_only_fields = ["commentID", "createdAt"]

class EditCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ["description"]


class ArticleSerializer(serializers.ModelSerializer):

    reactions = ReactionSerializer(many=True, required=False, source="reaction_set")
    comments = CommentSerializer(many=True, required=False, source="comment_set")

    class Meta:
        model = Article
        fields = ["articleID", "authorID", "title", "body", "createdAt", "category", "reactions", "comments", "isActive"]
        read_only_fields = ['articleID']
        depth = 1

    def create(self, validated_data):
        article = Article.objects.create(**validated_data)
        article.save()
        return article


class EditArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['title', 'body', 'category', 'isActive']


class CreateArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['title',  "body", "category"]
    
    def create(self, validated_data):
        article = Article.objects.create(**validated_data)
        article.save()
        return article