from rest_framework import serializers
from .models import *
from django.utils import timezone

class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = '__all__'
        read_only_fields = ["ID", "subscribedAt", "dueTo"]
        
class CreateSubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = ["period"]
    
    def validate_period(self, value):
        if value in [1, 3, 12]:
            return value
        else:
            raise serializers.ValidationError("Subscription periods are 1, 3 or 12 months")
        
class ModifySubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = ['payedOff', 'active']
    
    def update(self, instance, validated_data):
        instance.payedOff = validated_data.get('payedOff', instance.payedOff)
        instance.active = validated_data.get('active', instance.active)
        instance.save()
        return instance
    
class PaymentSerializer(serializers.Serializer):
    subscription = serializers.IntegerField(required=True)
    payedAmount = serializers.FloatField(required=True)
    payedAt = serializers.DateTimeField(default=timezone.now())
    
    def validate_subscription(self, value):
        try:
            sub = Subscription.objects.get(ID=value)
        except Subscription.DoesNotExist as e:
            raise serializers.ValidationError("Subscription of given id does not exist")
        return value