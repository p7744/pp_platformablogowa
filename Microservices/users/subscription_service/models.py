from user_service.models import User
from datetime import datetime
import random
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone

# Create your models here.

class Subscription(models.Model):
    ID = models.BigAutoField(primary_key=True)
    dueTo = models.DateTimeField(null=False)
    subscribedAt = models.DateTimeField(default=timezone.now)
    period = models.IntegerField(null=False)
    amount = models.FloatField(null=False)
    payedOff = models.BooleanField(default=False)
    active = models.BooleanField(null=False, default=False)
    userId = models.ForeignKey(User, on_delete=models.CASCADE)
    
    