from django.core.mail import send_mail
from .models import Subscription, models
import datetime
from django.utils import timezone
from django.core.mail import send_mail

def expire_old_subscriptions():
    oldtime = timezone.now() - datetime.timedelta(minutes=1)
    subs = Subscription.filter(subscribedAt__lte = oldtime)
    for sub in subs:
        sub.active = False
        sub.userId.is_vip = False
        sub.save()
        send_mail(
            'Blog Platform - Vip subscription expired',
            f'Your vip account has expired :(' +
                f'Please consider activating subscription again.\n' +
                f'You can do it in your account settings under "VIP subscription" tab.\n' +
                f'\n' +
                f'Regards from Blog Platform Team!',
                f'subscriptions@blogplatform.eu',
                [sub.userId.email],
                fail_silently=False
        )