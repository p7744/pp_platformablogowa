from ast import Sub
from django.shortcuts import get_object_or_404, render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import SessionAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
import datetime
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django.core.mail import send_mail
# Create your views here.

class SubscriptionViewSet(viewsets.ViewSet):
    
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    
    amounts = {
        1: 20,
        3: 50,
        12: 180 
    }
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Get subscriptions by logedin user",
        operation_id="Get subscriptions by logedin user",
        operation_description="Get subscriptions by logedin user",
        responses={200: SubscriptionSerializer, 404: "User Not Found"},   
    )
    def get_user_subcriptions(self, request):
        user_mail = request.user.email
        subs = Subscription.objects.filter(userId__email = user_mail, active=True)
        serializer = SubscriptionSerializer(subs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Create subscription for logedin user",
        operation_description="Create subscription for logedin user",
        responses={200: SubscriptionSerializer, 404: "Subscription Not Found"},  
        request_body=CreateSubscriptionSerializer
    )
    def create_subscription(self, request):
        data = request.data
        _serializer = CreateSubscriptionSerializer(data=data)
        
        if _serializer.is_valid():
            if request.user.is_vip:
                return Response({"ErrorMessage": "User already subscribes to vip"},status=status.HTTP_400_BAD_REQUEST)
            
            sub: Subscription = Subscription(**_serializer.validated_data)
            sub.userId = request.user
            sub.dueTo = sub.subscribedAt + datetime.timedelta(days=30 * sub.period)
            sub.amount = self.amounts[sub.period]
            sub.save()
            request.user.is_vip = True
            request.user.save()
            
            return Response(SubscriptionSerializer(sub).data, status=status.HTTP_201_CREATED)
        return Response(_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class DetailSubcriptionViewSet(viewsets.ViewSet):
    
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]   
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Get subscription by id",
        operation_id="Get subscription by its id",
        operation_description="Get subscription by id",
        responses={200: SubscriptionSerializer, 404: "Subscription Not Found"},  
    )
    def get_object(self, request, pk):
        try:
            sub = Subscription.objects.get(ID = pk)
        except Subscription.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        return Response(SubscriptionSerializer(sub).data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Delete subscription by id",
        operation_description="Delete subscription by id",
        responses={204: "No Content", 404: "Subscription Not Found"},  
    )
    def delete_object(self, request, pk):
        try:
            sub = Subscription.objects.get(ID=pk)
        except Subscription.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        sub.active = False
        request.user.is_vip = False
        request.user.save()
        sub.save()
        
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Update subscription by id",
        operation_description="Update subscription by id",
        responses={202: SubscriptionSerializer, 404: "Subscription Not Found"},  
        request_body=ModifySubscriptionSerializer
    )
    def modify_object(self, request, pk):
        try:
            sub = Subscription.objects.get(ID=pk)
        except Subscription.DoesNotExist as e:
            return Response(data={"ErrorMessage": "Object not found"}, status=status.HTTP_404_NOT_FOUND)
        
        serializer = ModifySubscriptionSerializer(sub, data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
        sub = serializer.save()        
        return Response(SubscriptionSerializer(sub).data, status=status.HTTP_200_OK)
        
        
class PaymentsViewSet(viewsets.ViewSet):
    
    @swagger_auto_schema(
        tags=["subscriptions"],
        name="Pay for subscription",
        operation_description="Pay for subscription",
        responses={200: "Payed for subscription", 400:"Incorrect data sent", 404: "Subscription Not Found"},  
        request_body=PaymentSerializer
    )
    def pay_for_subscription(self, request):
        serializer = PaymentSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        sub_pk = serializer.data.get("subscription")
        try:
            sub = Subscription.objects.get(ID=sub_pk)
            sub.dueTo = timezone.now() + datetime.timedelta(days=30 * sub.period)
            sub.payedOff = True
            sub.active = True
            sub.save()
        except Subscription.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
        send_mail(
            'Blog Platform - Vip subscription activated',
            f'Thank you for activating your subcription :)' +
                f'You have successfully paid for your subscription.\n' +
                f'This subscription will be active for the next {sub.period * 30} days.\n' +
                f'It wil expire on {sub.dueTo},\nwe will inform you after it will expire so you can activate a new subscription.\n\n' +
                f'Regards from Blog Platform Team!',
                f'subscriptions@blogplatform.eu',
                [sub.userId.email],
                fail_silently=False
        )
        
        return Response({"Message": "Payed your subcription correctly"}, status=status.HTTP_200_OK)