from django.contrib import admin

# Register your models here.
from .serializers import *
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAdminUser
from rest_framework import viewsets, generics
from rest_framework.authentication import SessionAuthentication


'''
ADMIN RELATED VIEWS
'''
class AllUsersViewSet(viewsets.ModelViewSet):    
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = [JWTAuthentication, SessionAuthentication]
    permission_classes = [IsAdminUser]
    
class SingleUserViewSet(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = [JWTAuthentication, SessionAuthentication]
    permission_classes = [IsAdminUser]
    
    
from .models import *

admin.site.register(User)
