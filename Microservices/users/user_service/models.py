from datetime import datetime
import random
from xmlrpc.client import DateTime
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone
# Create your models here.

class UserManager(BaseUserManager):
    """
    User manager to create new django user
    """
    def create_user(self, email, password, **fields):
        """Creates django user

        :param email: _description_
        :type email: _type_
        :param password: _description_
        :type password: _type_
        :raises ValueError: _description_
        :raises ValueError: _description_
        :return: _description_
        :rtype: _type_
        """
        if not email:
            raise ValueError("Email and login must be set")
        email = self.normalize_email(email)
        
        if (datetime.now().year - fields.get("birthday").year) < 13:
            raise ValueError('User\' age should be 13 or older')
        
        user = self.model(email=email, **fields)
        user.set_password(password)
        user.save()
        return user
    
    def create_superuser(self, email, password, **fields):
        """Creates superuser for admin page

        :param email: _description_
        :type email: _type_
        :param password: _description_
        :type password: _type_
        :param birthday: _description_
        :type birthday: datetime
        :raises ValueError: _description_
        :raises ValueError: _description_
        :return: _description_
        :rtype: _type_
        """
        fields.setdefault('is_active', True)
        fields.setdefault('is_staff', True)
        fields.setdefault('is_superuser', True)
        
        if fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        
        if fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        
        fields['birthday'] = datetime.strptime("1999-10-05T14:48:00+0200", "%Y-%m-%dT%H:%M:%S%z")
        
        return self.create_user(email, password, **fields)
            

def profile_picture_path(instance, filename):
    """Returns server path to save user's profile picture

    :param instance: instance of a new user created before this method is run
    :type instance: User
    :param filename: Name of an uploaded file
    :type filename: str
    :return: A path to the media folder that profile picture will be save in
    :rtype: str
    """
    return '/'.join([f'profilep_{str(instance.email).replace("@", "_").replace(".", "_")}', filename])

class User(AbstractBaseUser, PermissionsMixin):
    login = models.TextField(null=False, unique=True)
    email = models.EmailField(null=False, unique=True)
    is_active = models.BooleanField(default=False, null=False)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    birthday = models.DateField(null=False)
    description = models.TextField(max_length=200, null=True)
    profile_picture = models.ImageField(upload_to=profile_picture_path, null=True)
    is_vip = models.BooleanField(default=False)
    mail_auth_token = models.TextField(default=str(random.randrange(100000, 999999)),max_length=6)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    objects = UserManager()
    
    def __str__(self):
        return f"{self.login}, {self.email}"
    

    