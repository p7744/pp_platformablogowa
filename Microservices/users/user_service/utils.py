from rest_framework.permissions import BasePermission

class IsCreationOrIsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        
        if not request.user.is_authenticated:
            print(view.action)
            if view.action == 'create_new_user' or view.action == 'mail_auth':
                print("test")
                return True
            else:
                return False
        else:
            return True