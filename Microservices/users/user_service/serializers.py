from rest_framework import serializers
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["pk", "email", "password", "login", "birthday", "profile_picture", "description", "is_vip", "is_active", "mail_auth_token"]
        
    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        return user
    
class NewUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [ 'email', 'password', "login", 'birthday', 'profile_picture']
        
    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        return user

class MailTokenAuthSerializer(serializers.Serializer):
    token = serializers.CharField(min_length=6, max_length=6)
    
class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "login", "description", "profile_picture"]
        
class ModifyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["profile_picture", "password", "description"]
        extra_kwargs = { "password": {"required": False}}
