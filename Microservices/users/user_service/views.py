from ast import Mod
import stat
from django.shortcuts import get_object_or_404, render
from django.views import View
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import *
from .models import *
from .utils import *
from rest_framework import generics 
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import SessionAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.core.mail import send_mail
from rest_framework.parsers import MultiPartParser
from django.core.paginator import Paginator

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
# Create your views here.

DEFAULT_PER_PAGE  = 10

class UserViewSet(viewsets.ViewSet):
    
    serializer = UserSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsCreationOrIsAuthenticated,]
    parser_classes = (MultiPartParser,)
    
    # def get_permissions(self):
    #     return [permission() for ]
    
    def mailAuthentication(self, mail, token):
        send_mail(
            'Blog Platform - Mail activation token',
            f'Thank you for creating your account' +
                f'This is your activation token: {token}\n' +
                f'Activate on this website http://127.0.0.1:3000/activateUser/{mail} \n' +
                f'Regards from Blog Platfor Team!',
                f'activation@blogplatform.eu',
                [mail],
                fail_silently=False
        )
    
    @swagger_auto_schema(
        tags=["users"],
        name="Get loged in user",
        operation_description="Returns logedin user",
        responses={200: UserSerializer, 404: 'User not found'},
        permission_classes=[IsCreationOrIsAuthenticated],
        authentication_classes=[JWTAuthentication]
    )
    def get_user(self, request):
        try:
            user = User.objects.get(email=request.user.email)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(self.serializer(user).data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        tags=["users"],
        name="Create user",
        operation_description="Creates a new user for the app",
        responses={201: UserSerializer, 400: 'Incorrectly sended data'},
        request_body=NewUserSerializer,
        permission_classes=[IsCreationOrIsAuthenticated],
        authentication_classes=[JWTAuthentication]
        
    )
    def create_new_user(self, request):
        data = request.data
        _serializer = NewUserSerializer(data=data)
        if _serializer.is_valid():
            new_user = _serializer.create(_serializer.validated_data)
            try:
                new_user.save()
            except Exception as e:
                return Response({"ErrorMessage": e}, status=status.HTTP_400_BAD_REQUEST)
            self.mailAuthentication(new_user.email, new_user.mail_auth_token)
            return Response(UserSerializer(new_user).data, status=status.HTTP_201_CREATED)
        return Response(data=_serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    
    @swagger_auto_schema(
        tags=["users"],
        name="Mail authentication",
        operation_description="Activates user with provided activation token",
        responses={202: "User auctivated", 400: 'Incorrectly sended data'},
        request_body=MailTokenAuthSerializer,
        permission_classes=[IsCreationOrIsAuthenticated],
        authentication_classes=[JWTAuthentication]
        
    )
    def mail_auth(self, request, mail):
        serializer = MailTokenAuthSerializer(data=request.data)
        if serializer.is_valid():
            user = User.objects.filter(email=mail).first()
            if user.is_active:
                return Response(data={"ErrorMessage": "User already active"}, status=status.HTTP_400_BAD_REQUEST)
            real_token = user.mail_auth_token
            print(real_token)
            if real_token != serializer.validated_data.get('token'):
                return Response(data={"ErrorMessage": "Incorrect token"}, status=status.HTTP_400_BAD_REQUEST)
            
            user.is_active = True
            user.save()
            return Response(data={"Message": "Activated user"}, status=status.HTTP_202_ACCEPTED)
        
        return Response(data={"ErrorMessage": "Incorrect request data"}, status=status.HTTP_400_BAD_REQUEST)
    
    @swagger_auto_schema(
        tags=["users"],
        name="Update user data fields",
        operation_description="Modify an existing logedin user for the app",
        responses={202: "Accepted", 400: 'Incorrectly sended data'},
        request_body=ModifyUserSerializer,
        permission_classes=[IsCreationOrIsAuthenticated],
        authentication_classes=[JWTAuthentication]
    )
    def modify_user(self, request):
        usr_email = request.user.email
        user = User.objects.get(email=usr_email)
        
        data = request.data
        _serializer = ModifyUserSerializer(data=data)
        if _serializer.is_valid():
            valid_data = _serializer.validated_data
            for k, v in valid_data.items():
                if k == "password":
                    print(v)
                    if v is not None and v != "":
                       user.set_password(v) 
                elif k != "password" and v is not None:
                    setattr(user, k, v)
            try:
                user.save()
            except Exception as e:
                return Response({"ErrorMessage": e}, status=status.HTTP_400_BAD_REQUEST)
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(data=_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                    
    @swagger_auto_schema(
        tags=["users"],
        name="Delete user",
        operation_description="Deletes an existing logedin user for the app",
        responses={204: "NO CONTENT"},   
        permission_classes=[IsCreationOrIsAuthenticated],
        authentication_classes=[JWTAuthentication]
    )
    def delete_user(self, request):
        usr_email = request.user.email
        user = User.objects.get(email=usr_email)
        user.is_active = False
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    

class ProfileViewSet(viewsets.ViewSet):
    
    serializer_class = UserProfileSerializer
    
    page_param = openapi.Parameter('page', openapi.IN_QUERY, description="Number of Page", type=openapi.TYPE_INTEGER)
    per_page_param = openapi.Parameter('per_page', openapi.IN_QUERY, description="Number of profiles per page", type=openapi.TYPE_INTEGER)
    
    @swagger_auto_schema(
        tags=["profiles"],
        name="Get all profiles",
        operation_description="Gets all profiles",
        responses={200: UserProfileSerializer, 404: "Page number exceeded"},
        manual_parameters=[page_param, per_page_param]
    )
    def get_all(self, request):
        profiles = User.objects.filter(is_active=True).order_by('-created_at')
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(profiles, per_page=int(per_page))
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            profiles = paginator.page(int(page))
            serializer = self.serializer_class(profiles, many = True)
            return Response({
                "count": len(profiles),
                "next": (f"http://127.0.0.1:8000/api/v1/profiles?page={int(page)+1}&per_page={per_page}" if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8000/api/v1/profiles?page={int(page)-1}&per_page={per_page}" if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        return Response(data=self.serializer_class(profiles, many=True).data, status=status.HTTP_200_OK) 
    
    @swagger_auto_schema(
        tags=["profiles"],
        name="Get profile by login or id",
        operation_description="Get profile by login or id",
        responses={200: UserProfileSerializer, 404: "User Not Found"},   
    )
    def get_object(self, request, usr_login):
        try:
            profile = User.objects.filter(is_active=True, login=usr_login).first()
            if profile is None:
                usr_id: int
                try:
                    usr_id = int(usr_login)
                    profile = User.objects.get(is_active=True, pk=int(usr_login))
                except:
                    return Response(status=status.HTTP_404_NOT_FOUND)

        except User.DoesNotExist as e:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(self.serializer_class(profile).data, status=status.HTTP_200_OK)

