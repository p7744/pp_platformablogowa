"""users URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_simplejwt import views as jwt_views
from subscription_service.views import PaymentsViewSet
from observation_service.views import *

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


urlpatterns = [
    path('api/v1/users/admin/', admin.site.urls),
    path('api/v1/', include('user_service.urls')),
    path('api/v1/subscriptions', include('subscription_service.urls')),
    path('api/v1/auth', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/auth/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/auth/verify', jwt_views.TokenVerifyView.as_view(), name='token_verify'),
    path('api/v1/payments', PaymentsViewSet.as_view({'post': 'pay_for_subscription'})),
    path('api/v1/observations', ObservationViewSet.as_view({"get": "get_all", "post": "create"})),
    path('api/v1/observers', ObserversViewSet.as_view({"get": "get_all"})),
    path('api/v1/observations/<pk1>&<pk2>', ObservationViewSet.as_view({"get": "get_one", "delete": "delete_one"})),
    path('api/v1/observers/<pk1>&<pk2>', ObserversViewSet.as_view({"get": "get_one"})),
] 
schema_view = get_schema_view(
   openapi.Info(
      title="Blogplatform user service API",
      default_version='v1',
      description="API for blogplatform user service",
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns += [
   re_path(r'^api/v1/users/swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   re_path(r'^api/v1/users/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   re_path(r'^api/v1/users/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
