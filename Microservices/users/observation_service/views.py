from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import SessionAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.core.paginator import Paginator
from .permissions import *
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
#Create your views here.

DEFAULT_PER_PAGE = 10

class ObservationViewSet(viewsets.ViewSet):
    
    serializer_class = ObservationSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsGetOrIsAuthenticated,]
    
    @swagger_auto_schema(
        tags=["observations"],
        name="Get all observations",
        operation_id="Get all observations",
        operation_description="Get all observaltions",
        responses={200: ObservationSerializer(many=True)},   
    )
    def get_all(self, request):
        observs = Observation.objects.all()
        
        queries = ""
        
        if userID := request.GET.get("user_id"):
            observs = observs.filter(userId__pk = userID )
            if observs is None:
                return Response(status=status.HTTP_404_NOT_FOUND)
            queries += f"&user_id={userID}"
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(observs, per_page=int(per_page))
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            observs = paginator.page(int(page))
            serializer = self.serializer_class(observs, many = True)
            return Response({
                "count": len(observs),
                "next": (f"http://127.0.0.1:8000/api/v1/observations?page={int(page)+1}&per_page={per_page}" + queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8000/api/v1/observations?page={int(page)-1}&per_page={per_page}" + queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        return Response(data=self.serializer_class(observs, many=True).data, status=status.HTTP_200_OK) 
    
    @swagger_auto_schema(
        tags=["observations"],
        name="Get one observation by its id",
        operation_description="Get one observaltion by its ids",
        operation_id="Get one observation by its id",
        responses={200: ObservationSerializer, 404: "Not Found"},   
    )
    def get_one(self, request, pk1, pk2):
        try:       
            observation = Observation.objects.get(userId__login=pk1, observatedUser__login=pk2)
        except Observation.DoesNotExist as e:
            return Response({"ErrorMessage": "Such observation does not exist"}, status=status.HTTP_404_NOT_FOUND)

        serializer = ObservationSerializer(observation)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
    
    @swagger_auto_schema(
        tags=["observations"],
        name="Create one observation",
        operation_description="Create one observation",
        request_body=ObservationSerializer,
        responses={201: ObservationSerializer, 400: "Bad Request"},   
    )
    def create(self, request):
        data = request.data
        data['userId'] = request.user.pk
        serializer = ObservationSerializer(data=data)
        if not serializer.is_valid():
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        
        new_observation = Observation(**serializer.validated_data)
        new_observer = Observer(observingUser = new_observation.userId, userId = new_observation.observatedUser)
        new_observation.save()
        new_observer.save()
        return Response(ObservationSerializer(new_observation).data, status.HTTP_201_CREATED)
        
    
    @swagger_auto_schema(
        tags=["observations"],
        name="Delete one observation by its id",
        operation_description="Delete one observaltion by its ids",
        responses={204: "No Content", 404: "Not Found"},   
    )
    def delete_one(self, request, pk1, pk2): 
        try:       
            observation = Observation.objects.get(userId__pk=pk1, observatedUser__pk=pk2)
        except Observation.DoesNotExist as e:
            return Response({"ErrorMessage": "Such observation does not exist"}, status=status.HTTP_404_NOT_FOUND)
        
        try:       
            observer = Observer.objects.get(userId__pk=pk2, observingUser__pk=pk1)
        except Observation.DoesNotExist as e:
            return Response({"ErrorMessage": "Such observer does not exist"}, status=status.HTTP_404_NOT_FOUND)

        observation.delete()
        observer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
        
            
class ObserversViewSet(viewsets.ViewSet):
    
    serializer_class = ObserversSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsGetOrIsAuthenticated,]
    
    @swagger_auto_schema(
        tags=["observers"],
        name="Get all observers",
        operation_description="Get all observers",
        responses={200: ObserversSerializer},   
    )
    def get_all(self, request):
        observs = Observer.objects.all()
        queries = ""
        
        if userID := request.GET.get("user_id"):
            observs = observs.filter(userId__pk = userID )
            if observs is None:
                return Response(status=status.HTTP_404_NOT_FOUND)
            queries += f"&user_id={userID}"
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(observs, per_page=int(per_page))
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            observs = paginator.page(int(page))
            serializer = self.serializer_class(observs, many = True)
            return Response({
                "count": len(observs),
                "next": (f"http://127.0.0.1:8000/api/v1/observers?page={int(page)+1}&per_page={per_page}" + queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8000/api/v1/observers?page={int(page)-1}&per_page={per_page}" + queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        return Response(data=self.serializer_class(observs, many=True).data, status=status.HTTP_200_OK) 
    
    @swagger_auto_schema(
        tags=["observers"],
        name="Get one observer by its ids",
        operation_description="Get on observer by its ids",
        responses={200: ObserversSerializer, 404: "Not Found"},   
    )
    def get_one(self, request, pk1, pk2):
        try:       
            observer = Observer.objects.get(observingUser__login=pk1, userId__login=pk2)
        except Observer.DoesNotExist as e:
            return Response({"ErrorMessage": "Such observation does not exist"}, status=status.HTTP_404_NOT_FOUND)

        serializer = ObserversSerializer(observer)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    # @swagger_auto_schema(
    #     tags=["observers"],
    #     name="Delete one observer by its ids",
    #     operation_description="Delete one observer by its ids",
    #     responses={204: "No Content", 404: "Not Found"},   
    # )
    # def delete_one(self, request, pk1, pk2):
    #     try:       
    #         observer = Observer.objects.get(observingUser__login=pk1, userId__login=pk2)
    #     except Observer.DoesNotExist as e:
    #         return Response({"ErrorMessage": "Such observation does not exist"}, status=status.HTTP_404_NOT_FOUND)

    #     observer.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)