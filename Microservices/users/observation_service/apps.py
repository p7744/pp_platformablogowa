from django.apps import AppConfig


class ObservationServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'observation_service'
