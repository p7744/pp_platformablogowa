# Generated by Django 4.0.3 on 2022-04-11 09:01

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Observer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('observedAt', models.DateTimeField(default=datetime.datetime(2022, 4, 11, 9, 1, 2, 591677, tzinfo=utc))),
                ('observingUser', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='%(class)s_observing', to=settings.AUTH_USER_MODEL)),
                ('userId', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='%(class)s_observated', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('observingUser', 'userId')},
            },
        ),
        migrations.CreateModel(
            name='Observation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('observedAt', models.DateTimeField(default=datetime.datetime(2022, 4, 11, 9, 1, 2, 590612, tzinfo=utc))),
                ('observatedUser', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='%(class)s_observated', to=settings.AUTH_USER_MODEL)),
                ('userId', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='%(class)s_observing', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('userId', 'observatedUser')},
            },
        ),
    ]
