from rest_framework.permissions import BasePermission

class IsGetOrIsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        
        if not request.user.is_authenticated:
            if view.action in ['get_all', 'get_one']:
                return True
            else:
                return False
        else:
            return True