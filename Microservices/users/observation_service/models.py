from datetime import datetime
import random
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager
from django.utils import timezone
from user_service.models import User

# Create your models here.
class Observation(models.Model):
    """Obserwacje uytkownika
    """
    class Meta:
        unique_together=(('userId', "observatedUser"),)
    
    userId = models.ForeignKey(User, models.DO_NOTHING, related_name="%(class)s_observing")
    observatedUser = models.ForeignKey(User, models.DO_NOTHING, related_name="%(class)s_observated")
    observedAt = models.DateTimeField(default=timezone.now)

class Observer(models.Model):
    """Obserwujący uytkownika
    """
    class Meta:
        unique_together=(('observingUser', "userId"),)
    
    observingUser = models.ForeignKey(User, models.DO_NOTHING, related_name="%(class)s_observing")
    userId = models.ForeignKey(User, models.DO_NOTHING, related_name="%(class)s_observated")
    observedAt = models.DateTimeField(default=timezone.now)
