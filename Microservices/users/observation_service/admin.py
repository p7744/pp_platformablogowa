from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Observation)
admin.site.register(Observer)