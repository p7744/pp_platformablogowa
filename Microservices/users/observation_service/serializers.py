from rest_framework import serializers
from .models import *

class ObservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Observation
        fields = '__all__'
   

class ObserversSerializer(serializers.ModelSerializer):
    class Meta:
        model = Observer
        fields = '__all__'
        read_only_fields = ["observingUser", "userID"]
        