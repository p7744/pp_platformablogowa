from django.db import models
from django.utils import timezone

def nameFile(instance, filename):
    return '/'.join(['images', str(instance.articleID), filename])


class Multimedia(models.Model):
    class Meta:
        unique_together = (('articleID', 'multimediaID'),)

    articleID = models.IntegerField()
    multimediaID = models.BigAutoField(primary_key=True)
    multimediaPath = models.ImageField(null=True, blank=True, upload_to=nameFile)
    MultimediaType = models.CharField(max_length=100)

    uploadedAt = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
