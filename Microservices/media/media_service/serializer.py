import json
from .models import *
from rest_framework import serializers
from requests import get

class MultimediaSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Multimedia
        fields = ('multimediaID', 'articleID','multimediaPath', 'MultimediaType', 'uploadedAt')
        read_only_fields = ["multimediaID", 'uploadedAt', 'MultimediaType']
        
    def create(self, validatedData):
        multimedia = Multimedia(**validatedData)
        return multimedia
    
    def validate_articleID(self,value):
        try:
            int(value)
        except:
            raise serializers.ValidationError("articleID has to be an int value")
        
        validation_rq = get("http://article-service:8001/api/v1/articles/{}".format(value))
        if validation_rq.status_code >= 400:
            raise serializers.ValidationError("article of the id {} does not exist".format(value))
        
        return value
        
