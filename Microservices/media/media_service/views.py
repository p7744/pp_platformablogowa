from django.shortcuts import render
from rest_framework.response import Response

from.models import *

from .serializer import *
from django.core.paginator import Paginator
from rest_framework import viewsets, status
from .permissions import *


DEFAULT_PER_PAGE = 10

class MultimediasViewSet(viewsets.ViewSet):

    permission_classes = [JWTTokenVerificationPermission]

    def get_multimedia(self, request):
        medias = Multimedia.objects.all()
        
        additional_queries = ""
        
        if a_article := request.GET.get('article_id'):
            medias = medias.filter(articleID = a_article)
            if not medias:
                return Response(status=status.HTTP_404_NOT_FOUND)
            additional_queries += f"&article_id={a_article}" 
        
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        
        if page:
            per_page = int(per_page) if per_page else DEFAULT_PER_PAGE
            paginator = Paginator(medias, per_page=per_page)
            if int(page) > paginator.num_pages:
                return Response(status=status.HTTP_404_NOT_FOUND)
            medias = paginator.page(int(page))
            serializer = MultimediaSerializer(medias, many = True)
            return Response({
                "count": len(medias),
                "next": (f"http://127.0.0.1:8002/api/v1/media?page={int(page)+1}&per_page={per_page}" + additional_queries \
                    if paginator.num_pages > int(page) else None),
                "previous": (f"http://127.0.0.1:8002/api/v1/media?page={int(page)-1}&per_page={per_page}" + additional_queries \
                    if int(page) > 1 else None),
                "results": serializer.data
            }, status=status.HTTP_200_OK)
        
        serializer = MultimediaSerializer(medias, many = True)
        return Response(serializer.data, status = status.HTTP_200_OK)

    def create_new_multimedia(self, request):
        data = request.data
        serializer = MultimediaSerializer(data=data)
        if serializer.is_valid():
            new_media = serializer.create(serializer.validated_data)
            new_media.save()
            return Response(MultimediaSerializer(new_media).data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete_multimedia(self, request, multimediaID):
        try:
            multimediaToRemove = Multimedia.objects.get(multimediaID=multimediaID)
            multimediaToRemove.delete()
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
            

        return Response(status=status.HTTP_204_NO_CONTENT)
    
    def get_by_id(self, request, multimediaID):
        media = Multimedia.objects.filter(multimediaID = multimediaID).first()
        if not media:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(MultimediaSerializer(media).data, status=status.HTTP_200_OK)
