from rest_framework.permissions import BasePermission
from media import settings
from requests import post
import jwt

class JWTTokenVerificationPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method == "GET":
            return True
        
        if (authorization_token := request.headers.get('Authorization')):
            # token
            token = authorization_token.replace("Bearer ", "")
            auth_request = post("http://user-service:8000/api/v1/auth/verify", data={'token': token})
            if auth_request.status_code == 200:
                user = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
                # user_id
                request.userid = user.get("user_id")
                return True
        return False