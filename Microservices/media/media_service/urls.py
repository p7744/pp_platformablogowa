import re
from django.contrib import admin
from django.urls import path, include
from .views import *
from rest_framework import routers




urlpatterns = [
    path('', MultimediasViewSet.as_view({'get': 'get_multimedia', 'post': 'create_new_multimedia'})),
    path('/<int:multimediaID>', MultimediasViewSet.as_view({"get": "get_by_id", "delete": "delete_multimedia"}))
]
