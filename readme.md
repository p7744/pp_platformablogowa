# Platforma Blogowa - Projekt Programistyczny

Jest to repozytorium GIT dla kodu aplikacji Platforma Blogowa

Autorzy:
* Daniel Świetlik
* Adam Ćwikła
* Aleksander Mickiewicz
* Kamil Tlałka
* Jan Słowik
* Tomasz Stefaniak

Aplikację można bardzo łatwo uruchomić przez dockera i funkcję docker-compose z głównego katalogu repozytorium

## 2. Podział obowiązków w grupie:

Osoba | Obowiązki | Opis
--- | --- | ---
Daniel Świetlik | Dokumentacja, Backend| Przygotowanie dokumentacji projektu a także mikroserwisu do logowania/rejestracji/modyfikacji kont użytkowników. Konfiguracja projektu i jego konteneryzacja.
Adam Ćwikła | Testy jednostkowe - backend | Przygotowanie testów jednostkowych dla api systemu oraz pomoc przy serwisie dla artykułu
Jan Słowik | Frontend | Przygotowanie ekranu logowania, rejestracji i ustawień konta użytkownika
Kamil Tlałka | Frontend | Przygotowanie interface'u tworzenia i publikacji artykułu oraz wyświetlania i interakcji z nim
Aleksander Mickiewicz | Backend| Przygotowanie mikroserwisu encji artykułu 
Tomasz Stefaniak | Frontend | Przygotowanie interface'u dla wyświetlania artykułów, wyszukiwania ich oraz wyświetlania profilu bloggera.

