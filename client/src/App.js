import logo from './logo.svg';
import RegistrationPageMemo from './components/Registration'
import ActivateUser from './components/ActivateUser'
import FormularzPostu from './components/FormularzPostu';
import './App.css';
import NotFoundView from './components/NotFound';
import * as React from 'react';
import * as ReactDOM from "react-dom";
import { useState, useEffect } from 'react';
import MainPage from './components/MainPage';
import Login from './components/Login'
import ButtonAppBar from "./components/NavigationBar";
import UserProfile from  './components/UserProfile';
import SearchPage from './components/SearchPage';

import { Routes, Route, useNavigate } from 'react-router-dom'
import {Hook} from 'react-router-dom'
import PersistLogin from "./components/PersistLogin";
import RequireAuth from "./components/RequireAuth";
import AccountEditView from "./components/AccountEditView";
import AnnoyingAdd from './components/AnnoyingAdd';
import useAuth from "./hooks/useAuth";
import PostView from "./components/WidokPostu";
import EditPostForm from "./components/EditPostForm";

export const SnapbarMessageTypes = {
    Success: 'success',
    Error: 'error',
    Warrning: 'warrning'
}

function App() {

  

  return (
      <div className="App">
        <ButtonAppBar />
        <Routes >
          <Route element={<PersistLogin />}>

            <Route exact path='/' element={ <MainPage /> }/>
            <Route exact path='/login' element={ <Login /> }/>
            <Route exact path='/register' element={ <RegistrationPageMemo /> }/>
            <Route exact path='/activateUser/:email' element={<ActivateUser />}/>
            <Route exact path='/404' element={<NotFoundView />} />
            <Route path='/*' element={<NotFoundView />} />

          <Route exact path='/a/:articleId' element={<PostView />} />
          <Route exact path='/userProfile/:login' element={<UserProfile />}/>

              <Route element={<RequireAuth />}>
                  <Route exact path='/editArticle/:articleId' element={<EditPostForm />} />
              </Route>

            <Route element={<RequireAuth />}>
              <Route exact path='/createArticle' element={<FormularzPostu />}/>
            </Route>
            <Route element={<RequireAuth />}>
              <Route exact path="/accountSettings" element={<AccountEditView />} />
            </Route>
              <Route exact path='/search' element={<SearchPage/>}/>

          </Route>

        </Routes>
        
      </div>
  );
}

export default App;
