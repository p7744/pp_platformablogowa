import * as React from 'react';
import SnackbarContext from "../context/SnackbarProvider";

export const SnapbarMessageTypes = {
    Success: 'success',
    Error: 'error',
    Warrning: 'warrning'
}

 function useSnackbar () {
     const { message, setMessage } = React.useContext(SnackbarContext);

    const createNewMessage = async (messageType, messageText) => {
        setMessage({messageOpen: true, messageType: messageType, messageText: messageText});
    }

    return createNewMessage;
}

export default useSnackbar;