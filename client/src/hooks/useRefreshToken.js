import useAuth from './useAuth';
import UserService from "../services/UserService";
import ObservationService from "../services/ObservationService";

export default function useRefreshToken() {
    const { auth, setAuth } = useAuth();

    const refresh = async function() {
        const userService = new UserService();
        const observationsService = new ObservationService();
        if (!localStorage.getItem('refresh-token'))
            return Promise.reject();

        try {
            const response = await userService.refreshToken(localStorage.getItem('refresh-token'));
            console.log(response)
            let pk = null;
            let login = null;
            let profile_picture = null;
            let isVip = null;
            let usrnm = null
            let observations = null;
            if (response?.access) {
                try {
                    const resp2 = await userService.getUser(response?.access);
                    pk = resp2?.pk;
                    login = resp2?.email;
                    usrnm = resp2?.login;
                    profile_picture = `${process.env.REACT_APP_USER_SERVICE_IP}${resp2?.profile_picture}`
                    isVip = resp2?.is_vip;

                    observations = await observationsService.getObservations(null, null, pk);
                    console.log(resp2)
                    console.log(isVip)

                } catch (err2) {
                    return Promise.reject();
                }
            }

            setAuth(prev => {
                console.log(JSON.stringify(prev));
                console.log(response);
                return { ...prev, accessToken: response?.access, pk: pk, usrnm: usrnm, login: login, profile_picture: profile_picture, isVip: isVip,
                                observations: observations}
            })
        } catch (err) {
            return Promise.reject();
        }
    }

    return refresh;
}