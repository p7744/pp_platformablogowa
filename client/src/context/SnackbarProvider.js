import {createContext, useEffect, useState} from "react";
import {Alert, Snackbar} from "@mui/material";
import * as React from "react";

const SnackbarContext = createContext({});

export function SnackbarProvider({ children }) {
    const [message, setMessage] = useState({messageOpen: false, messageType: -1, messageText: ""});

    useEffect(() => {
        console.log(message);
    }, [message]);

    return (
        <SnackbarContext.Provider value={{message, setMessage}}>
            {children}

            {message.messageOpen && <Snackbar
                open={message.messageOpen}
                autoHideDuration={4000}
                onClose={() => {
                    setMessage({messageOpen: false, messageType: -1, messageText: ""});
                }}
            >
                <Alert onClose={() => {
                    setMessage({messageOpen: false, messageType: -1, messageText: ""});
                }} sx={{width: '100%'}} severity={message.messageType}>
                    {message.messageText}
                </Alert>
            </Snackbar>
            }
        </SnackbarContext.Provider>
    )
}

export default SnackbarContext;