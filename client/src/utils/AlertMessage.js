
import Alert from '@mui/material/Alert';

export const MESSAGE_TYPE = {
    NO_MS: -1,
    SUCCESS_MSG: 0,
    ERROR_MSG: 1,
    WARNING_MSG: 2,
    INFO_MSG: 3
}

export const MESSAGE_ALERT_TYPE = {
    0: "success",
    1: "error",
    2: "warning",
    3: "info"
}

export function AlertMessage({ alertType, alertMessage, onCloseCallback}) {
    

    return (
        <Alert severity={MESSAGE_ALERT_TYPE[alertType]} onClose={() => { onCloseCallback() }}>
        <strong>{alertMessage}</strong>
        </Alert>
    )
}