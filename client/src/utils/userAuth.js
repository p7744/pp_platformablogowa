export async function verifyUserToken(token) {
    if (typeof(token) === typeof(string))
        throw new Error("Type of token is not string")

    const verifyTokenData = {"token": `${token}`}
    let verifiedToken = false

    await fetch('http://127.0.0.1:8000/api/v1/auth/verify', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(verifyTokenData),
    })
    .then(response => {
      //console.log(response.status)
      if (response.status === 200) {
        verifiedToken = true
        //console.log(verifiedToken)
      }
      return response.json() 
    }).catch(error => {
      //verifiedToken = false
    })

    //console.log(verifiedToken)
    return verifiedToken
}

export async function refreshUserToken(token) {
    if (typeof(token) === typeof(string))
        throw new Error("Type of token is not string")

    const refreshTokenData = {"refresh": `${token}`}
    var accessToken = null
    var verifiedToken = false

    await fetch('http://127.0.0.1:8000/api/v1/auth/refresh', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(refreshTokenData),
    })
    .then(response => {
        if (response.status === 200) {
            verifiedToken = true
            //console.log("test")
        }
        return response.json() 
    })
    .then(data => {
        if (verifiedToken) {
            localStorage.setItem("token", data.access)
            accessToken = localStorage.getItem("token")
            //console.log(accessToken)
        }
    })
    
    return accessToken
}