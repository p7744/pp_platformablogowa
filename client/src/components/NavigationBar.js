import {useEffect, useState} from "react";
import {useLocation, useNavigate} from "react-router-dom";
import Box from "@mui/material/Box";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Home from "@mui/icons-material/Home";
import Typography from "@mui/material/Typography";
import SearchBar from "./SearchBar";
import { Avatar } from '@mui/material';
import AccountCircle from "@mui/icons-material/AccountCircle";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import * as React from "react";
import useAuth from "../hooks/useAuth";
import AddCommentIcon from '@mui/icons-material/AddComment';
import useSnackbar, {SnapbarMessageTypes} from "../hooks/useSnackbar";
import HelpCenterIcon from '@mui/icons-material/HelpCenter';


export default function ButtonAppBar() {

  const { auth, setAuth } = useAuth();
  const setSnackbar = useSnackbar();

  const [anchorEl, setAnchorEl] = useState(null);

  const navigate = useNavigate()
  const location = useLocation()

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };




  return (

    <Box sx={{ flexGrow: 1, marginBottom: 10 }}>
      <AppBar style={{backgroundColor: "black", paddingLeft: "20%", paddingRight: "20%"}}>
        <Toolbar>
          <Typography className="Title" variant="h6" component="div"
                      sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block',
                          color: 'white', fontFamily: "Brush Script MT",
                          fontSize: '25px'} }} onClick={() => { navigate('/')}}>
            Blog Platform
          </Typography>
          <IconButton size={"large"} arial-label={"Help for search"} color={"inherit"}
                      onClick={() => {
                        alert(`Help for search bar:
                          Use "#" prefix to search by category
                          Use "@" prefix to search by user
                          No prefix search by title
                        `);
                      }}

          >
              <HelpCenterIcon />
          </IconButton>
          <SearchBar/>
          <IconButton size="large" aria-label="Explore new posts" color="inherit" onClick={ () => { navigate("/") } }>
              <Home />
            </IconButton>
          { auth?.accessToken ? <div>
              <IconButton size="large" aria-label="Add new articles" color="inherit"
                onClick={() => { navigate("/createArticle") }}>
                <AddCommentIcon />
              </IconButton>

              <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <Avatar alt={`${auth?.login} profile picture`} src={auth?.profile_picture} />
            </IconButton> 
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={() => {navigate(`/userProfile/${auth?.usrnm}`); Location.reload() }}>Profile</MenuItem>
                <MenuItem onClick={() => {navigate("/accountSettings")}}>Account Settings</MenuItem>
                <MenuItem onClick={() => {
                                      setAnchorEl(null);
                                      localStorage.removeItem("refresh-token");
                                      setAuth({});
                                      setSnackbar(SnapbarMessageTypes.Success, "Logout successfuly");
                                    }}>Logout</MenuItem>
              </Menu>
            </div> : <Button color="inherit" onClick={() => {


              navigate('/login?redirect-to=' + location.pathname);
            }}>Login</Button> }
        </Toolbar>
      </AppBar>

    </Box>
  );
}