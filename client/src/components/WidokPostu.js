import React, {Component} from 'react';
import {useParams} from 'react-router-dom';
import './formstyle.css';
import ReactMarkdown from 'react-markdown'
import ReactDOM from 'react-dom';
import ArticleService from '../services/ArticleService';
import remarkGfm from 'remark-gfm';
import remarkMath from 'remark-math';
import rehypeKatex from 'rehype-katex';
import rehypeRaw from 'rehype-raw';
import rehypeHighlight from 'rehype-highlight'
import rehypeSanitize from 'rehype-sanitize';
import 'katex/dist/katex.min.css';
import useAuth from '../hooks/useAuth';
import useSnackbar, {SnapbarMessageTypes} from '../hooks/useSnackbar';
import {Navigate, useNavigate} from 'react-router-dom';
import MediaService from '../services/MediaService';
// import "github-markdown-css";
import {Modal, Box, IconButton, Button, CircularProgress} from '@mui/material';
import {FormatItalic, FormatBold, AddPhotoAlternate, Title} from "@mui/icons-material";
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter'
import {a11yDark as vs} from 'react-syntax-highlighter/dist/esm/styles/prism'
import UserService from "../services/UserService";
import {ThumbDown, ThumbUp, ThumbUpOutlined, ThumbDownOutlined} from "@mui/icons-material";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import CardActions from "@mui/material/CardActions";
import TextField from "@mui/material/TextField";
import * as Colors from "./ColorPallete";
import {Grid, Avatar} from "@mui/material";
import {red} from "@mui/material/colors";
import CommentService from "../services/CommentService";
import ReactionService from "../services/ReactionService";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';


const UserReactionState = {
    DIDNT_REACT: 0,
    DID_LIKED: 1,
    DID_DISLIKED: 2
};

function PostView() {

    const {auth} = useAuth();
    const setMessage = useSnackbar();

    const _articleService = new ArticleService();
    const _userService = new UserService();
    const navigate = useNavigate();

    const [loaded, setLoaded] = React.useState(false);
    const [article, setArticle] = React.useState({});
    const [author, setAuthor] = React.useState({});
    const [convertedArticle, setConvertedArticle] = React.useState("");
    const [comment, setComment] = React.useState("");
    const [comments, setComments] = React.useState([]);
    const [didChange, setDidChange] = React.useState(false);
    const [userReactionState, setUserReactionState] = React.useState(0);
    const [reaction, setReaction] = React.useState({});

    const {articleId} = useParams();


    const addReaction = async (type) => {
        let liked = false;

        if (type == "LIKE") {
            liked = true;
        } else if (type == "DISLIKE") {
            liked = false;
        }
        const reactionService = new ReactionService();

        try {
            let react = await reactionService.addReaction(auth?.accessToken, articleId, liked);
            if (react == null) {
                setMessage(SnapbarMessageTypes.Error, "Could not add your reaction,");
                return;
            }

            if (liked)
                setUserReactionState(UserReactionState.DID_LIKED);
            else
                setUserReactionState(UserReactionState.DID_DISLIKED);
            setReaction({...react})
        } catch (err) {
            setMessage(SnapbarMessageTypes.Error, "Could not react");
        }

    };

    const deleteReaction = async (type, id) => {
        const reactionService = new ReactionService();
        try {
            let res = await reactionService.deleteReaction(auth?.accessToken, id);

            setReaction({});
            setUserReactionState(UserReactionState.DIDNT_REACT);

        } catch (err) {
            setMessage(SnapbarMessageTypes.Error, "Could not delete reaction");
        }
    };

    const deleteArticle = async () => {
        console.log("Heloo")
        const articleService = new ArticleService();

        await articleService.deleteArticle(auth?.accessToken, articleId)
            .then(res => {
                setMessage(SnapbarMessageTypes.Success, "Article was deleted successfuly");
                navigate("/userProfile/" + auth?.usrnm);
            })
            .catch(err => {
                console.error(err);
                setMessage(SnapbarMessageTypes.Error, "Could not delete this article");
            });
    };

    const editArticle = async () => {
        navigate("/editArticle/" + articleId + "?redirect-to=" + "/a/" + articleId);
    };

    const addComment = async () => {
        const commentService = new CommentService();
        commentService.addComment(auth?.accessToken, articleId, comment)
            .then(r => {
                setComment("");
                setMessage(SnapbarMessageTypes.Success, "Published new comment");
                setDidChange(!didChange);
            })
            .catch(e => {
                console.error(e);
                setMessage(SnapbarMessageTypes.Error, "Could not pubish your comment")
            })

    };


    React.useEffect(() => {
        console.log(articleId)

        const fetchData = async () => {
            let result = {}
            try {
                result = await _articleService.getArticleById(articleId);
            } catch (err) {
                window.reload();
            }
            if (result == null || result?.isActive == false) {
                navigate("/404");
            }
            let user = {};
            try {
                user = await _userService.getProfileByLogin(result?.authorID);
            } catch (err) {
                window.reload();
            }
            let reaction = null;
            try {
                reaction = await (new ReactionService()).getReactions(null, null, auth?.pk, articleId);
                if (reaction != null)
                    reaction = reaction[0]
                console.log(reaction)
                if (reaction?.liked == true) {
                    setUserReactionState(UserReactionState.DID_LIKED);
                } else if (reaction?.liked == false) {
                    setUserReactionState(UserReactionState.DID_DISLIKED);
                } else {
                    setUserReactionState(UserReactionState.DIDNT_REACT);
                }
                setReaction({...reaction});
            } catch (err) {
                window.reload();
            }


            setLoaded(true);
            setConvertedArticle(`# ${result?.title} \n<br> <br>\n\n` + result?.body);
            setArticle({...result});
            setAuthor({...user})

            try {
                let comms = await (new CommentService().getComments(null, null, null, articleId));
                console.log(comms)
                if (comms) {
                    for (let i = 0; i < comms.length; i++) {
                        try {
                            let author = await _userService.getProfileByLogin(comms[i]['authorID']);
                            comms[i]['author'] = author;
                        } catch (err) {
                            comms[i]['author'] = null;
                        }

                    }
                    setComments([...comms]);
                    console.log([...comms])
                } else {
                    setComments([]);
                }
            } catch (err) {
                console.error(err)
            }

            console.log(article);
        };
        fetchData();

    }, [didChange])

    return (
        <div className={"WidokPostu"}>
            <header className="header">


            </header>
            <div className="leftpanel">


            </div>
            <div className="center" id="sub" style={{overflowY: "scroll"}}>
                <div className="markdown-body" id="markdownoutput">
                    {loaded === true ? <ReactMarkdown
                        remarkPlugins={[remarkGfm, remarkMath]}
                        rehypePlugins={[rehypeKatex, rehypeRaw]}
                        components={{
                            code({node, inline, className, children, ...props}) {
                                const match = /language-(\w+)/.exec(className || '')
                                return !inline && match ? (
                                    <SyntaxHighlighter
                                        children={String(children).replace(/\n$/, '')}
                                        style={vs}
                                        language={match[1]}
                                        PreTag="div"
                                        {...props}
                                    />
                                ) : (
                                    <code className={className} {...props}>
                                        {children}
                                    </code>
                                )
                            }
                        }}
                    >
                        {convertedArticle}
                    </ReactMarkdown> : <CircularProgress style={{position: 'relative', left: "50%"}}/>}  </div>

                {auth?.accessToken != null ?
                    (<><CardActions>
                            <IconButton aria-label="add to favorites"
                                        onClick={(e) => {
                                            if (userReactionState === UserReactionState.DID_LIKED)
                                                deleteReaction("LIKE", reaction?.reactionID)
                                            else if (userReactionState !== UserReactionState.DID_DISLIKED)
                                                addReaction("LIKE")
                                            else {
                                                const changeReact = async () => {
                                                    await deleteReaction("DISLIKE", reaction?.reactionID)
                                                    await addReaction("LIKE");
                                                }
                                                changeReact();
                                            }

                                        }}>
                                {userReactionState === UserReactionState.DID_LIKED ? <ThumbUp/> : <ThumbUpOutlined/>}
                            </IconButton>
                            <IconButton aria-label="add to not favorites"
                                        onClick={(e) => {
                                            if (userReactionState === UserReactionState.DID_DISLIKED)
                                                deleteReaction("DISLIKE", reaction?.reactionID)
                                            else if (userReactionState !== UserReactionState.DID_LIKED)
                                                addReaction("DISLIKE")
                                            else {
                                                const changeReact = async () => {
                                                    await deleteReaction("LIKE", reaction?.reactionID)
                                                    await addReaction("DISLIKE");
                                                }
                                                changeReact();
                                            }
                                        }}>
                                {userReactionState === UserReactionState.DID_DISLIKED ? <ThumbDown/> :
                                    <ThumbDownOutlined/>}
                            </IconButton>
                            <IconButton aria-label="share">
                                <ShareIcon
                                    onClick={() => {
                                        navigator.clipboard.writeText("http://localhost/a/" + articleId + "?source=browserShare")
                                        setMessage(SnapbarMessageTypes.Success, "Copied link to the clipboard")
                                    }}
                                />
                            </IconButton>
                        </CardActions>
                            <CardActions style={{height: "fit-content"}}>
                                <div style={{
                                    display: "flex",
                                    borderTop: `1px solid ${Colors.BORDER_COLOR}`,
                                    width: "100%"
                                }}>
                                    <TextField id="standard-basic" label="Add comment..." value={comment}
                                               variant="standard" onChange={(e) => {
                                        setComment(e.target.value)
                                    }}
                                               style={{width: "88%"}}/>
                                    <Button variant="text"
                                            onClick={addComment}
                                    >Publish</Button>
                                </div>
                            </CardActions></>
                    )
                    : ""}
                <div style={{
                    backgroundColor: "white", width: "100%", overflow: "scroll", overflowX: "hidden",
                    paddingTop: "20px", paddingBottom: "20px"
                }}>
                    <h2>Comments:</h2>
                    {comments?.length > 0 ? comments.map(c => {


                        return (
                            <CommentView
                                userLogin={c.author?.login ?? "deleted"}
                                userPP={c.author?.profile_picture ?? " "}
                                comment={c.description}
                                date={c.createdAt}
                                tags={c.tags}
                                commId={c.commentID}
                                auth={auth}
                                handleRefresh={setDidChange}
                                didChange={didChange}
                            />
                        )
                    }) : "No comments..."}
                </div>


            </div>

            <div className="rightpanel" style={{backgroundColor: "#ffffff"}}>
                <h3>Autor:</h3>
                <img className="zdjprof" id="zdjeciepreview" src={`${process.env.REACT_APP_USER_SERVICE_IP}${author?.profile_picture}`}
                     alt="your image"
                     style={{cursor: "pointer", border: "1px solid #DBDBDB"}}
                     onClick={() => navigate("/userProfile/" + author?.login)}
                />
                <h4>{author?.login}</h4>
                <div style={{textAlign: "left", padding: "5%"}}>
                    Upload Date: {new Date(article?.createdAt).toDateString()}
                    <br></br><br></br>
                    Category: <strong>{article?.category}</strong>
                    <br></br><br></br>
                </div>
                {auth?.usrnm == author?.login && (
                    <>
                        <Button variant="contained" onClick={editArticle}>Edit Article</Button>
                        <br></br><br></br>
                        <Button variant="contained" color="error" onClick={deleteArticle}>Delete Article</Button>
                    </>)
                }

            </div>


            <footer className="footer">
            </footer>


        </div>


    )

}

function CommentView(props) {

    const setMessage = useSnackbar();
    const navigate = useNavigate();

    const deleteComment = async (access, id) => {
        await (new CommentService()).deleteComment(props.auth?.accessToken, props.commId)
            .then(res => {
                props.handleRefresh(!props.didChange);
            })
            .catch(err => {
                setMessage(SnapbarMessageTypes.Error, "Could not delete this comment");
            });
    };

    return (
        <Grid container spacing={0.1} sx={{marginTop: "4%", marginLeft: "4%", marginRight: "10%"}}>
            <Grid item xs={2}>
                <Avatar sx={{bgcolor: red[500]}} aria-label="recipe" src={`${process.env.REACT_APP_USER_SERVICE_IP}${props.userPP}`}
                        onClick={() => {
                            navigate(`/${props.userLogin}`)
                        }} alt={props.userLogin}
                        sx={{cursor: "pointer"}}
                />
            </Grid>
            <Grid item xs={8} sx={{overflowWrap: "anywhere", textAlign: "left", fontSize: '12px', paddingRight: "4%"}}>
                <strong>{props.userLogin}</strong> {props.comment}
                {props.tags != null ? (
                    <>
                        <br></br><br></br>
                        {props.tags}
                        <br></br>
                    </>
                ) : ""}
                <br></br>
                {new Date(props.date).toDateString()}
            </Grid>
            <Grid item xs={1}>
                {props.auth?.usrnm == props.userLogin &&
                    (
                        <IconButton
                            onClick={() => deleteComment(props.auth?.accessToken, props.commId)}
                        >
                            <DeleteForeverIcon color="error"/>
                        </IconButton>
                    )
                }

            </Grid>
        </Grid>
    );
}


export default PostView

