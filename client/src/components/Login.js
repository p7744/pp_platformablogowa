import './Login.css';
import { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import * as React from 'react';
import Link from '@mui/material/Link';
import { useNavigate } from 'react-router-dom'
import useAuth from "../hooks/useAuth";
import UserService from "../services/UserService";
import ObservationService from "../services/ObservationService";
import useQuery from "../hooks/useQuery";


const LoginPage = (props) => {

    const navigate = useNavigate()
    const query = useQuery();

    const { setAuth } = useAuth();

    const [success, setSuccess] = useState(false);

    React.useEffect(() => {
        if (success) {
            let to;
            if ((to = query.get("redirect-to")) != null) {
                navigate(to);
            } else {
                navigate("/");
            }
        }

    }, [success])

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [errorMsg, setErrorMsg] = useState("");
    const [errorOcc, setErrorOcc] = useState(false);


   const signin = async(e) => {
       e.preventDefault();

        const userService = new UserService();
        const observationsService = new ObservationService();
        try {
            const response = await userService.logUserIn(login, password);
            console.log(response);

            const accessToken = response?.access;
            const refreshToken = response?.refresh;

            let pk = null;
            let usrnm = null;
            let profile_picture = null;
            let isVip = null;
            let observations = null;

            if (accessToken) {
                try {
                    const resp2 = await userService.getUser(accessToken);
                    pk = resp2?.pk;
                    usrnm = resp2?.login;
                    profile_picture = `${process.env.REACT_APP_USER_SERVICE_IP}${resp2?.profile_picture}`
                    isVip = resp2?.is_vip;

                    observations = await observationsService.getObservations(null, null, pk);

                } catch (err2) {
                    console.error(err2);
                    setErrorOcc(true);
                    setErrorMsg("Something went wrong while getting a user");
                    return null;
                }
            }
            setAuth({ login, password, pk, usrnm, profile_picture, isVip, accessToken, refreshToken, observations });
            localStorage.setItem("refresh-token", refreshToken);
            setSuccess(true);
            setLogin("");
            setPassword("");
        } catch (err) {
            console.error(err);
            setErrorOcc(true);
            setErrorMsg("Incorrect email or password. Please try again.");
        }

   }

    return (
        <div className="LoginPage">
            { errorOcc ? 
                <Alert severity="error" onClose={() => { setErrorOcc(false) }}><strong>{errorMsg}</strong></Alert> : <div className="error-spaceing"></div>
                }
            <div className="loginForm">
                
                <h1>Login</h1>
                <TextField className="textField" id="outlined-basic" label="Email" variant="outlined" onChange={event => setLogin(event.target.value)}/>
                <TextField
                    className="textField"
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    onChange={event => setPassword(event.target.value) }
                />
                <Button className="login-button" variant="contained" onClick={signin}>Login</Button>
                <Link href="#" underline="hover" onClick={ () => {navigate('/register')} }>
                    {'Does not have an account? Click here'}
                </Link>
            </div>           
        </div>
    );
};

export default LoginPage;