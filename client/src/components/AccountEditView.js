
// export default function AccountEditView(props) {
//
//     return (
//         <div className="AccountEditView">
//             <h1>AccountEditView</h1>
//         </div>
//     )
// }

import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import UserService from "../services/UserService";
import SubscriptionService from "../services/SubscriptionService";
import useAuth from "../hooks/useAuth";
import IconButton from "@mui/material/IconButton";
import {Avatar, Button, Grid, Input} from "@mui/material";
import styled from 'styled-components';
import TextField from "@mui/material/TextField";
import useSnackbar, {SnapbarMessageTypes} from "../hooks/useSnackbar";
import {useNavigate} from "react-router-dom";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Stack from '@mui/material/Stack';
import Modal from '@mui/material/Modal';
import { SupervisorAccountSharp } from '@mui/icons-material';



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


function EditData(props) {

  const {auth, setAuth} = useAuth();
  const setSnackbar = useSnackbar();
  const navigate = useNavigate();
  const user = props.user;

  const profilePictureDiv = styled.div`
    display: flex;
    flex-direction: row;
    justify-items: start;
    border: 1px solid black;
    width: 100%;
    height: 30%;
  `;

  const NameAndButtonDiv = styled.div`
    display: flex;
    flex-direction: column;
    justify-items: start;
  `

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };


    const [prevPassword, setPrevPassword] = React.useState("")
    const [newPassword, setNewPassword] = React.useState(null)
    const [confirm, setConfirm] = React.useState("")
    const [desc, setDesc] = React.useState("")
    const [currentPhoto, setCurrentPhoto] = React.useState(null)
    const [photoToSend, setPhotoToSend] = React.useState(null);
    const [successChange, setSuccessChange] = React.useState(false);

  React.useEffect(() => {
      setCurrentPhoto(auth?.profile_picture)
      new UserService().getProfileByLogin(auth?.usrnm)
        .then(r => setDesc(r?.description))
  }, [])

  React.useEffect(() => {
        console.log(currentPhoto)
  }, [currentPhoto])

  React.useEffect(() => {
        // Just to force reloading of component
  }, [setSuccessChange])

    const [passError, setPassError] = React.useState(false);
    const [passErrorMsg, setPassErrorMsg] = React.useState("");

    const [confPassError, setConfPassError] = React.useState(false);
    const [confPassErrorMsg, setConfPassErrorMsg] = React.useState("");

    const [prevError, setPrevError] = React.useState(false);
    const [prevErrMsg, setPrevErrMsg] = React.useState("");

    

  const saveChanges = async (e) => {
      const userService = new UserService();
        let verify = null;
      try {
          verify = await userService.logUserIn(auth?.login, prevPassword);
      } catch (err) {
          setPrevErrMsg("Incorect password, try again");
          setPrevError(true);
          Promise.reject();
          return;
      }

      let passToChange = null;
      
      if (newPassword != null && !passError && !confPassError) {
          passToChange = newPassword;
      }

      let res = false;
      try {
        console.log(auth?.accessToken)
        res = await userService.modifyUser(verify?.access, photoToSend, passToChange, desc);
      } catch (err) {
        console.log(err);
        setSnackbar(SnapbarMessageTypes.Error, "Could not verify user. Try loging out and loging in again.")
        return;
      }

      if (!res) {
        setSnackbar(SnapbarMessageTypes.Error, "Could not modify your user data. Try again.")
      }

      setSuccessChange(!successChange);
      
      setSnackbar(SnapbarMessageTypes.Success, "Sucessfuly modified your data")
      window.location.reload();
  }

  const closeAccount = async (e) => {
      const userService = new UserService();
      let verify = null;
      console.log(auth?.login)
      console.log(prevPassword)
      try {
        verify = await userService.logUserIn(auth?.login, prevPassword);
      } catch (err) {
        setPrevErrMsg("Incorect password, try again");
        setPrevError(true);
        return;
      }
    
      
      if (verify?.access == null) {
        setPrevErrMsg("Incorect password, try again");
        setPrevError(true);
        return;
      }

      setPrevErrMsg("");
      setPrevError(false);
      handleClickOpen();
  }

  const handleCloseAccount = async (e) => {
      const userService = new UserService();

      let res = await userService.deleteUser(auth?.accessToken);

      if (!res) {
          setSnackbar(SnapbarMessageTypes.Error, "Could not close account, Some internal error occured.")
      }

      localStorage.removeItem("refresh-token");
      setAuth({});
      setSnackbar(SnapbarMessageTypes.Success, "Successfuly closed your account.")
      navigate("/")
  };

  return (
      <div className={"EditData"}>
          <Grid container spacing={4} >
              <Grid item xs={6}>
                  <label htmlFor="contained-button-file">
                      <Input accept="image/*" id="contained-button-file" type="file" style={{display: "none"}}
                            onChange={(e) => {
                                let { files } = e.target;
                                setPhotoToSend(files[0]);
                                const reader = new FileReader();
                                reader.onload = (e) => {
                                    const { result } = e.target;
                                    setCurrentPhoto(result);
                                }
                                reader.readAsDataURL(files[0]);
                            }}/>
                      <IconButton
                          component={"span"}
                        size="large"
                        edge="end"
                        aria-label="Profile picture change"
                        aria-haspopup="true"
                        color="inherit"
                        helperText={"Change your profile picture"}
                      >
                        <Avatar alt={`${auth?.login} profile picture`} src={currentPhoto} />
                      </IconButton>
                </label>
                  <p style={{fontSize: 12}}>Change profile picture</p>
              </Grid>
              <Grid item xs={6}>
                  <TextField id="description" placeholder="Description"
                                          multiline helperText={"Profile biography"}
                                         minRows={3} value={desc}
                                         maxRows={3} onChange={(e) => {setDesc(e.target.value)}}
                                       variant="outlined" sx={{width: '100%'}} style={{minHeight: '50%'}}/>
              </Grid>
              <Grid item xs={6}>
                  <p ><strong>Username</strong></p>
              </Grid>
              <Grid item xs={6}>
                  <TextField id="outlined-basic" label={auth?.usrnm} variant="outlined" disabled style={{width: '100%'}}/>
              </Grid>
              <Grid item xs={6}>
                  <p ><strong>Previous password</strong></p>
              </Grid>
              <Grid item xs={6}>
                  <TextField id="outlined-basic" label=""
                             placeholder="Previous password" variant="outlined" style={{width: '100%'}}
                            type={"password"} error={prevError} helperText={prevErrMsg}
                            onChange={(e) => {setPrevPassword(e.target.value)}}
                  />
              </Grid>
              <Grid item xs={6}>
                  <p ><strong>New password</strong></p>
              </Grid>
              <Grid item xs={6}>
                  <TextField id="outlined-basic" label="" placeholder="New password"variant="outlined" style={{width: '100%'}}
                     onChange={e => {
                                 var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!\(\)@#\$%\^&\*\-])(?=.{8,})");
                                 setNewPassword(e.target.value);
                                    if (!strongRegex.test(e.target.value)) {
                                        setPassError(true);
                                        setPassErrorMsg('Password must contain 1 lower case, 1 upper case, 1 special character and be 8 chars or longer')
                                    } else {
                                        setPassError(false);
                                        setPassErrorMsg('');
                                    }
                             }}
                             type={"password"}
                             error={passError}
                             helperText={passErrorMsg}
                  />
              </Grid>
              <Grid item xs={6}>
                  <p ><strong>Confirm new password</strong></p>
              </Grid>
              <Grid item xs={6}>
                  <TextField id="outlined-basic" label="" placeholder="Confirm new password" variant="outlined" style={{width: '100%'}}
                  type={"password"}
                    error={confPassError}
                    helperText={confPassErrorMsg}
                  onChange={e => {
                      setConfirm(e.target.value);
                      if (e.target.value != newPassword) {
                          setConfPassError(true);
                          setConfPassErrorMsg("Passwords doesn't match!")
                      } else {
                          setConfPassError(false);
                          setConfPassErrorMsg("");
                      }
                  }}
                  />
              </Grid>
              <Grid item xs={6}>
                  <Button variant="contained" onClick={saveChanges}>Save changes</Button>
              </Grid>
              <Grid item xs={6}>
                  <Button variant="contained" color="error" onClick={closeAccount}>Close account</Button>
              </Grid>
          </Grid>

          <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Closing account"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Your are about to close your account. Are you REALLY sure you want to do this?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleCloseAccount} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      </div>
  )
}

function SubscriptionView(props) {

  const _subService = new SubscriptionService();

  const { auth, setAuth } = useAuth();
  const setSnackbar = useSnackbar();

  const [subscription, setSubscription] = React.useState(null);

  const [success, setSuccess] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
      const foo = async () => {
        let response = null;
        try {
          response = await _subService.getSubscriptions(auth?.accessToken)
        } catch (err) {
          setSnackbar(SnapbarMessageTypes.Error, "Something went wrong, try reloading this page");
        }

        if (response != null && response.length == 1) {
            setSubscription(response[0]);
        } else {
            setSubscription(null);
        }
      }
      foo();

  }, [success]);

  const getPeriod = (period) => {
      switch (period) {
        case 1:
          return "1 month"
        case 3:
          return "3 months"
        case 12:
          return "12 months"
      }
  }

  const handleResign = async () => {
      try {
          let res = await _subService.resignSubcriptionWithId(auth?.accessToken, subscription?.ID);
          console.log(res);
          if (!res) {
            setSnackbar(SnapbarMessageTypes.Error, "Could not resign from subscription. Try again.")
          }
          else {
            setSnackbar(SnapbarMessageTypes.Success, "Resigned successfully.")
            setAuth(prev => {
              return {...prev, isVip: false}
            })
            setSuccess(!success);
            
          }

      } catch (err) {
        setSnackbar(SnapbarMessageTypes.Error, "Could not verify user please try loging out and in")
      }
  };

  return (
      <Box className={"SubscriptionView"}
          sx={{
            width: "100%",
            height: "100%",
            
          }}
          >
          { subscription != null ? (
              <Card sx={{ minWidth: 275, minHeight: 200}} style={{display: "flex", flexDirection: "column", justifyContent: "space-between"}}>
              <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  Current VIP subscription
                </Typography>
                <Typography variant="h5" component="div">
                  {
                    getPeriod(subscription?.period)
                  }
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  {`Due to: ${new Date(subscription?.dueTo.toString()).toDateString()}`}
                </Typography>
                <Typography variant="body2">
                  You will not see any adds for the period
                  <br />
                  of this VIP subscription.
                </Typography>
              </CardContent>
              <CardActions>
                <Button color="error" size="small" onClick={handleResign}>Resign</Button>
              </CardActions>
            </Card>
          ) : 
          (
            <>
            
                <Stack
                  direction="column"
                  justifyContent="center"
                  alignItems="center"
                  spacing={2}
                >
                  <Typography>Do not have an active VIP subscription</Typography>
                  <Button variant="contained" onClick={(e) => { setOpen(true) }}>Subscribe now.</Button>
                </Stack>
                <NewSubscriptionModal
                  open={open}
                  onClose={() => setOpen(false) }
                  didSucceed={() => setSuccess(!success)}
                />
            </>
          )}
      </Box>
  )
}

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '90%',
  height: '60%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 2,
};

function NewSubscriptionModal({open, onClose, didSucceed}) {

  const {auth, setAuth} = useAuth();
  const setSnack = useSnackbar();
  
  const handleSubcription = async (time, amount) => {
      const subscriptionService = new SubscriptionService();

      let period = null;

      switch(time) {
        case 1:
          period = SubscriptionService.SUBSCRIPTION_PERIODS.oneMonth;
          break;
        case 3:
          period = SubscriptionService.SUBSCRIPTION_PERIODS.threeMonths;
          break;
        case 12:
          period = SubscriptionService.SUBSCRIPTION_PERIODS.oneYear;
          break;
      }
      let result = null;
      try {
         result = await subscriptionService.subscribeUser(auth?.accessToken, period);
      } catch (err) {
        console.log(err)
        setSnack(SnapbarMessageTypes.Error, "Could not subscribe, try again.")
        return;
      }

      if (result == null) {
        setSnack(SnapbarMessageTypes.Error, "Could not subscribe, you already have active subcription.")
        return;
      }

      const id = result?.ID;

      try {
        let result = await subscriptionService.makePayments(auth?.accessToken, id, amount);

        if (!result) {
          setSnack(SnapbarMessageTypes.Error, "Something went wront with peyments, we will rollback changes");
          return;
        }

        setSnack(SnapbarMessageTypes.Success, "Subcribed succesfully");
        setAuth(prev => {
          return {...prev, isVip: true}
        })
        onClose();
        didSucceed();
      } catch (err) {
        setSnack(SnapbarMessageTypes.Error, "Something went wront with peyments, we will rollback changes");
        return;
      }
      
  }

  return (
    <div>
      <Modal
        keepMounted
        open={open}
        onClose={onClose}
        aria-labelledby="keep-mounted-modal-title"
      >
        <Box sx={style}>
          <Typography id="keep-mounted-modal-title" variant="h6" component="h2" sx={{marginBottom: "40px"}}>
            Choose your plan
          </Typography>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <Card sx={{ minWidth: 255,  minHeight: 150 }} style={{display: "flex", flexDirection: "column", justifyContent: "space-between"}}>
              <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  1 month plan
                </Typography>
                <Typography variant="h5" component="div">
                  $2.99
                </Typography>
                <Typography variant="body2">
                  VIP gives you an addblock functionality
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" onClick={() => { handleSubcription(1, 2.99) }}>Subscribe</Button>
              </CardActions>
            </Card>
            <Card sx={{ minWidth: 255,  minHeight: 150 }} style={{display: "flex", flexDirection: "column", justifyContent: "space-between"}}>
              <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  3 month plan
                </Typography>
                <Typography variant="h5" component="div">
                  $7.99
                </Typography>
                <Typography variant="body2">
                  VIP gives you an addblock functionality
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" onClick={() => { handleSubcription(3, 7.99) }}>Subscribe</Button>
              </CardActions>
            </Card>
            <Card sx={{ minWidth: 255, minHeight: 150 }} style={{display: "flex", flexDirection: "column", justifyContent: "space-between"}}>
              <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  12 months
                </Typography>
                <Typography variant="h5" component="div">
                  $29.99
                </Typography>
                <Typography variant="body2">
                  VIP gives you an addblock functionality
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" onClick={() => { handleSubcription(12, 29.99) }}>Subscribe</Button>
              </CardActions>
            </Card>
          </Stack>
        </Box>
      </Modal>
    </div>
  );
}


export default function AccountEditView(props) {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const userService = new UserService();
  const subService = new SubscriptionService();

  const { auth } = useAuth();
  const [user, setUser] = React.useState({});


  React.useEffect(() => {
       userService.getUser(auth?.accessToken)
           .then(r => setUser(r))
           .catch(e => console.error(e))
    }, [])

  return (
    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex',
        height: '70%', width: '65%',
        position: 'absolute', transform: "translate(25%, 20%)",
        boxShadow: "0px 0px 5px 0px black"}}
    >
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider', width: "20%" }}
      >
        <Tab label="Account data" {...a11yProps(0)} />
        <Tab label="Subscriptions" {...a11yProps(1)} />
      </Tabs>
      <TabPanel style={{maxWidth: "80%"}} value={value} index={0}>
        <EditData user={user}/>
      </TabPanel>
      <TabPanel style={{width: "80%"}} value={value} index={1}>
        <SubscriptionView />
      </TabPanel>
    </Box>
  );
}
