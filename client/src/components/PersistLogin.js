import { Outlet } from 'react-router-dom';
import { useState, useEffect } from 'react';
import useRefeshToken from '../hooks/useRefreshToken';
import useAuth from '../hooks/useAuth';

export default function PersistLogin() {
    const [isLoading, setIsLoading] = useState(true);
    const refresh = useRefeshToken();
    const { auth } = useAuth();

    useEffect(function() {
        const verifyRefreshToken = async function () {
            try {
                await refresh();
            } catch (err) {
                console.error(err);
            }
            finally {
                setIsLoading(false);
            }
        }
        if (!auth?.accessToken)
            verifyRefreshToken();
        else
            setIsLoading(false);

    }, []);

    useEffect(function() {
        console.log(`isLoading: ${isLoading}`)
        console.log(`aT: ${JSON.stringify(auth?.accessToken)}`)
    }, [isLoading])

    return (
        <>
            {isLoading
                ? <p>Loading...</p>
                : <Outlet />
            }
        </>
    );
}