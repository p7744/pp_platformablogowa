import * as React from 'react';
import { useState, useEffect } from 'react';

import ArticlePost from './ArticlePost';
import ArticleService from './../services/ArticleService';
import './SearchPage.css';

import { useNavigate, useParams } from 'react-router-dom'
import UserService from './../services/UserService';
import MediaService from '../services/MediaService';
import useQuery from "../hooks/useQuery";


function displaySearchCategory(cat, auth, text){


    console.log(cat);
    return(<div>
            <h1> Wyszukujesz po </h1>
            {auth!="" && <h3>    Autorze: {auth!=""?auth:""} </h3> }
            {cat!="" && <h3>    Kategorii: {cat!=""?cat:""}  </h3>}
            {text!="" && <h3>    Tytule: {text!=""?text:""}  </h3>}
        </div>
    );

}


export default function SearchPage () {


    const [reloadData, setReloadData] = useState(false);
    let [searchValue, setSearchValue] = useState("");

    const _articleService = new ArticleService();
    const _userService = new UserService();
    const _mediaService = new MediaService();

    const query = useQuery();

    const [data, setData] = useState([])

    const [searchTag, setSearchTag] = useState()
    const [searchAuth, setSearchAuth] = useState()
    const [searchText, setSearchText] = useState()
    const [siftedArticle, setSiftedArticle] = useState([])

    const loadArticles = async () => {

        var searchInfo = query.get('query');

        if(searchInfo.includes("TAG-")){
            searchInfo = searchInfo.replace("TAG-","#");
        }

        if(searchInfo.includes("AUT-")){
            searchInfo = searchInfo.replace("AUT-","@");
        }

        if(searchInfo.includes("TIT-")){
            searchInfo = searchInfo.replace("TIT-","$");
        }

       var allProfiles = [];
       await _userService.getProfiles(1,null).then(data => allProfiles = data.results);
        //var searchInfo = "#Testowe";
        if (searchInfo == "") {
            return [];
        }
        var articles = [];

        await _articleService.getArticles(1, null)
                .then(data => { articles = data.results })
        if (articles == null) {
            return [];
        }


        var finalArts = [];
        setSearchAuth("");
        setSearchTag("");
        setSearchText("");

        var profile;

        if (!searchInfo.toLowerCase().startsWith("@") && !searchInfo.toLowerCase().startsWith("#")) {
            setSearchText(searchInfo);
            articles = articles.filter(article => {
                var compareString = article["title"].toString().toLowerCase();
                if(article["title"].toString().toLowerCase().includes(searchInfo.toLowerCase())){
                    finalArts.push(article);
                }
            });
        } else {
            for (let word of searchInfo.toLowerCase().split(' ')) {
                if (word == "") {
                    return;
                }
                if (word[0] == '#') {
                    setSearchTag(word.slice(1));
                    articles = articles.filter(article => {
                        var compareString = article["category"].toString().toLowerCase();
                        if(compareString == word.slice(1).toLowerCase()){
                            finalArts.push(article);
                        }
                    });
                }  else if(word[0] == '@'){
                    setSearchAuth(word.slice(1));
                    try {
                        profile = await _userService.getProfileByLogin(word.slice(1));
                    } catch (err) {}

                    articles = articles.filter(article => {
                        if(article.authorID == profile.id){
                            finalArts.push(article);
                        }
                    });

                }
            }
        }

            // getting artilce authors logins

            var selectedProfiles=[];
            for(var j = 0; j<allProfiles.length;j++)
            {
                finalArts.map((ar) => {if(ar.authorID == allProfiles[j].id){
                        selectedProfiles.push(allProfiles[j]);
                    }}
                );
            }

            let articles_to_save = finalArts;


            let i = 0;
            for (let article of finalArts) {
                var authorAsd
                try {
                    authorAsd = await _userService.getProfileByLogin(article["authorID"])
                }
                catch (eer){}

                articles_to_save[i]["authorID"] = authorAsd.login;
                articles_to_save[i]["profilePicture"] = authorAsd.profile_picture;


                await _mediaService.getMedias(null,null,article.articleID)
                    .then(data => {
                        if (data != null) {
                            articles_to_save[i]["Media"] = `${process.env.REACT_APP_MEDIA_SERVICE_IP}${data[0].multimediaPath}`;
                        }
                    });
                i++;
            }


        setSiftedArticle(articles_to_save);
        setData(articles_to_save);
    }

    useEffect(() => {
        if (query.get("query") !== searchValue) {
            setReloadData(!reloadData);
            console.log(query.get("query"))
        }
        setSearchValue(query.get("query"))
    }, []);

    useEffect(() => {
        const funnkcja = async () =>{
            await loadArticles()
        }
        funnkcja()
    },[reloadData])

    return (

    <div >
        <div className = "data">
            {displaySearchCategory(searchTag, searchAuth, searchText )}
        </div>

            <div className = "articles">
                    { data?.length > 0 ? data.map(art => {
                    console.log(art);
                return (
                    <div className="articleDiv">
                        <ArticlePost title = {art['title']}  name = {art['authorID']} media = {art['Media']} date = {art['createdAt']} articlePreview = {art['body'].slice(0, 100)} profilePicture = {art.profilePicture} articleId = {art.articleID} />
                    </div>
                )
            }) : <h3>Nic nie znaleziono</h3>}
            </div>
    </div>



    );
}

