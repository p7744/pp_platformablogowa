import * as React from 'react';

import './Popup.css';

const Popup = (p) => {
    return (
        <div className="popup">
            <div className="popup-background" onClick={p.handleClose}>
            </div>
            <div className="popup-box">
                {p.content}
            </div>
        </div>
    );
};

export default Popup;
