import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '90%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  

export default function AnnoyingAdd(props) {

    const add = require("../add.png");

    return (
        <div>
            <Modal
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            >
            <Box sx={style}>
                <img style={{height: '100%', width: '100%'}} 
                    alt="Annoying add" src={add}></img>
            </Box>
            </Modal>
            
        </div>
    )
}