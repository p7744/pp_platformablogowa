import './Registration.css';
import React from 'react';
import { useState, useEffect } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom'
import Link from '@mui/material/Link';
import Alert from '@mui/material/Alert';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import UserService from '../services/UserService';

function RegistrationPage(props) {

    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [dateOfBirth, setDateOfBirth] = useState(new Date())
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmedPassword] = useState("") 

    const [errorMsg, setErrorMsg] = useState("")
    const [errorOcc, setErrorOcc] = useState(false)

    const [passError, setPassError] = React.useState(false);
    const [passErrorMsg, setPassErrorMsg] = React.useState("");

    const [confPassError, setConfPassError] = React.useState(false);
    const [confPassErrorMsg, setConfPassErrorMsg] = React.useState("");

    const [loginError, setLoginError] = React.useState(false);
    const [loginErrorMsg, setLoginErrorMsg] = React.useState("");

    const navigate = useNavigate()

    const setULogin = async function(event) {
        setUsername(event.target.value)
    }

    const handleLoginError = async function(event) {
        if (username < 5) {
            setLoginError(true);
            setLoginErrorMsg("Login must be 5 or more character")
            return;
        }

        if ((await new UserService().getProfileByLogin(username)) != null) {
            setLoginError(true);
            setLoginErrorMsg("User of this login already exists");
            return
        }

        setLoginError(false);
        setLoginErrorMsg("");
    }

    const setUEmail = async function(event) {
        setEmail(event.target.value)
    }
    const setUDateOfBirth = async function(event) {
        setDateOfBirth(event.target.value)
    }
    const setUPassword = async function(event) {
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!\(\)@#\$%\^&\*\-])(?=.{8,})");
        setPassword(event.target.value)
        if (!strongRegex.test(event.target.value)) {
            setPassError(true);
            setPassErrorMsg('Password must contain 1 lower case, 1 upper case, 1 special character and be 8 chars or longer')
        } else {
            setPassError(false);
            setPassErrorMsg('');
        }
    }
    const setUConfirmedPassword = async function(event) {
        setConfirmedPassword(event.target.value)
        if (event.target.value != password) {
            setConfPassError(true);
            setConfPassErrorMsg("Passwords doesn't match!")
        } else {
            setConfPassError(false);
            setConfPassErrorMsg("");
        }
    }


    const registerUser = async function(event) {
        if (password !== confirmPassword) {
            setErrorOcc(true)
            setErrorMsg("Password dont match")
            return
        }
        let error = false;

        const userService = new UserService();
        try {
            let res = await userService.addUser(email, username, password, dateOfBirth);
        } catch (err) {
            setErrorOcc(true)
            setErrorMsg(JSON.stringify("requestResponse"))
        }

        if (!error)
            setOpen(true) 
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleRedirect = function() {
        setOpen(false)
        navigate(`/activateUser/${email}`)
    }

    return (
        <div className="RegistrationPage">
            { errorOcc ? 
                <Alert severity="error" onClose={() => { setErrorOcc(false); setErrorMsg(""); }}><strong>{errorMsg}</strong></Alert> : <div className="error-spaceing"></div>
                }
            <div className="registerForm">
                
                <h1>Register</h1>
                <TextField className="textField" id="outlined-basic" label="Login" variant="outlined" onChange={setULogin}
                    error={loginError}
                    helperText={loginErrorMsg}
                   onBlur={handleLoginError}
                />
                <TextField type="email" className="textField" id="outlined-basic" label="Email" variant="outlined" onChange={setUEmail}/>
                <TextField hint="" type="date" className="textField" id="outlined-basic" label="" variant="outlined" onChange={setUDateOfBirth}/>
                <TextField
                    className="textField"
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    onChange={setUPassword}
                    error={passError}
                    helperText={passErrorMsg}
                />
                <TextField
                    className="textField"
                    id="outlined-password-input"
                    label="Confirm Password"
                    type="password"
                    autoComplete="current-password"
                    onChange={setUConfirmedPassword}
                    error={confPassError}
                    helperText={confPassErrorMsg}
                />
                <Button className="login-button" variant="contained" onClick={registerUser}>Register</Button>
                <Link href="#" underline="hover" onClick={ () => {navigate('/login')} }>
                    {'Have an account? Login here'}
                </Link>

                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                    {"You created Account!"}
                    </DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Your account has been created, but it is not activated.
                        To login to your account you have to activate it first.
                        On your email there will be a message containing token to activate it.
                        Would you like to be redirected to subsite to input your token?
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleClose}>Disagree</Button>
                    <Button onClick={handleRedirect} autoFocus>
                        Agree
                    </Button>
                    </DialogActions>
                </Dialog>

            </div>           
        </div>
    )
}
const RegistrationPageMemo = React.memo(RegistrationPage)
export default RegistrationPageMemo
