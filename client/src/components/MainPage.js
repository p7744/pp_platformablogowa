
import Button from '@mui/material/Button'
import { useState, useEffect } from 'react';
import './MainPage.css';
import ArticlePost from './ArticlePost';
import HorizontalView from './HorizontalView';

import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from 'react-router-dom'

import ArticleService from './../services/ArticleService';
import UserService from './../services/UserService';
import MediaService from '../services/MediaService';
import ReactionService from '../services/ReactionService';
import CommentService from '../services/CommentService';

import {CircularProgress, Grid, Paper} from "@mui/material";
import * as React from "react";
import Popup from "./Popup";
import useAuth from "../hooks/useAuth";
import AnnoyingAdd from './AnnoyingAdd';

const fabStyle = {
    position: 'absolute',
    bottom: '-20%',
    right: 20,
};

function addEditButton(loggedUser, pageUser, userObserver, navigate, setSnackbar, observedUser, setObservedUser, setAuth){
    if(loggedUser?.accessToken == null)
        return

    if(loggedUser.usrnm == pageUser.login)
        return

    return(
        <Button size="small" style={{marginTop: "13px", marginLeft: "20px"}}>Observe</Button>
    )
}


const API_IP = process.env.REACT_APP_USER_SERVICE_IP;

export default function MainPage() {

    // Serwisy
    const _articleService = new ArticleService();
    const _userService = new UserService();
    const _mediaService = new MediaService();
    const _reactionService = new ReactionService();
    const _commentService = new CommentService();
    // Custom propsy
    const {auth} = useAuth();
    const _navigate = useNavigate();
    // Staty
    const [observedArticles, setObservedArticles] = useState([])
    const [commentedArticles, setCommentedArticles] = useState([])
    const [reactedArticles, setReactedArticles] = useState([])
    const [latestArticles, setLatestArticles] = useState([])
    const [openAdd, setOpenAdd] = React.useState(false);
    const [didLoadObs, setDidLoadObs] = React.useState(false);
    const [didLoadCom, setDidLoadCom] = React.useState(false);
    const [didLoadLik, setDidLoadLik] = React.useState(false);
    const [didLoadLat, setDidLoadLat] = React.useState(false);
    // Metody
    async function getArticlesMediaAndProfiles(articleArr) {
        for (let i = 0; i < articleArr.length; i++) {
            let profile = await _userService.getProfileByLogin(articleArr[i]['authorID']);
            articleArr[i]['authorID'] = profile?.login ?? "deleted";
            articleArr[i]['profilePicture'] = profile?.profile_picture ?? null;

            let media = await _mediaService.getMedias(null,null,articleArr[i]['articleID']);
            if (media != null) {
                articleArr[i]['Media'] = `${process.env.REACT_APP_MEDIA_SERVICE_IP}${media[0]?.multimediaPath}`;
            }

        }
        return articleArr
    }

    async function loadObserved() {
        let observedArticlesTemp = [];
        for (let observed of auth?.observations) {
            try {
                observedArticlesTemp = [...observedArticlesTemp,
                    ...await _articleService.getArticles(null, null, observed?.observatedUser)];
            } catch (err) {

            }
        }

        observedArticlesTemp = observedArticlesTemp.sort((oA, oB) => {
            return new Date(oA?.observedAt) - new Date(oB.observedAt)
        })

        setObservedArticles(await getArticlesMediaAndProfiles(observedArticlesTemp));
        setDidLoadObs(true);
    }

    async function loadCommented() {
        let commentedArticlesT = [];
        try {
            commentedArticlesT = [...await _articleService.getArticles()];

            commentedArticlesT.sort((aA, aB) => {
                let diff = aB?.comments?.length - aA?.comments?.length;

                if (diff == 0)
                    diff = new Date(aB?.createdAt) - new Date(aA?.createdAt);

               return diff;
            });

            commentedArticlesT = await getArticlesMediaAndProfiles(commentedArticlesT);

        } catch (err) {

        }
        setCommentedArticles(commentedArticlesT)
        setDidLoadCom(true);
    }

    async function loadMostLiked() {
        let mostLiked = [];
        try {
            mostLiked = [...await _articleService.getArticles()];

            mostLiked.sort((aA, aB) => {
                const likedA = aA?.reactions
                    .filter(val => val?.liked == true)
                    .length;

                const likedB = aB?.reactions
                    .filter(val => val?.liked == true)
                    .length;

                let diff = likedB - likedA;

                if (diff == 0)
                    diff = new Date(aB?.createdAt) - new Date(aA?.createdAt);

                return diff;
            });

            mostLiked = await getArticlesMediaAndProfiles(mostLiked);

        } catch (err) {
            console.error(err);
        }
        setReactedArticles(mostLiked)
        setDidLoadLik(true);
    }

    async function loadLatest() {
        let latest = [];
        try {
            latest = [...(await _articleService.getArticles(1, 15))?.results];

            latest = await getArticlesMediaAndProfiles(latest);

        } catch (err) {
            console.error(JSON.stringify(err));
        }

        setLatestArticles(latest);
        setDidLoadLat(true);
    }

    function loadArticles() {
        // Pobranie obserwowanych użytkownika
        if (auth?.accessToken)
            loadObserved();
        loadCommented();
        loadMostLiked();
        loadLatest();
    }

    React.useEffect(() => {
        console.log(auth)
        if (!auth?.isVip) {
            setOpenAdd(true)
        }
        loadArticles();

    }, [])

    return (
        <div className="MainPage" style={{ height: '100%'}}>
            { auth?.accessToken && (
                <HorizontalView title = "Posty Obserwowanych" content = {
                    observedArticles.length > 0 ? (
                        observedArticles.map(art =>{
                            return (
                                <ArticlePost title = {art['title']}  name = {art['authorID']} media = {art['Media']}
                                    date = {art['createdAt']} articlePreview = {art['body'].slice(0, 100)}
                                    profilePicture = {art.profilePicture} articleId = {art.articleID} />
                            )
                        })
                    ) : (
                        didLoadObs !== true ? <CircularProgress sx={{position: "relative", left: "5%"}} /> : <p>No observed users</p>
                    )
                }/>
            )}

            <HorizontalView title = "Najczęściej komentowane" content = {
                commentedArticles.length > 0 ? (
                    commentedArticles.map(art =>{
                        return (<ArticlePost title = {art['title']}  name = {art['authorID']} media = {art['Media']}
                                             date = {art['createdAt']} articlePreview = {art['body'].slice(0, 100)}
                                             profilePicture = {art.profilePicture} articleId = {art.articleID} />)
                })) : (
                    didLoadCom !== true ? <CircularProgress sx={{position: "relative", left: "5%"}} /> : <p>No posts</p>
                )
                        }/>
            <HorizontalView title = "Najbardziej lubiane" content = {
                reactedArticles.length > 0 ? (
                    reactedArticles.map(art =>{
                        return (<ArticlePost title = {art['title']}  name = {art['authorID']} media = {art['Media']}
                                             date = {art['createdAt']} articlePreview = {art['body'].slice(0, 100)}
                                             profilePicture = {art.profilePicture} articleId = {art.articleID} />)
                })) : (
                    didLoadLik !== true ? <CircularProgress sx={{position: "relative", left: "5%"}} /> : <p>No posts</p>
                )
            }/>

            <HorizontalView title = "Najnowsze" content = {
                latestArticles.length > 0 ? (
                    latestArticles.map(art =>{
                        return (<ArticlePost title = {art['title']}  name = {art['authorID']}
                                             media = {art['Media']} date = {art['createdAt']}
                                             articlePreview = {art['body'].slice(0, 100)} profilePicture = {art.profilePicture}
                                             articleId = {art.articleID} />)
                    })
                ) : (
                   didLoadLat !== true ? <CircularProgress sx={{position: "relative", left: "5%"}} /> : <p>No posts</p>
                )

            }/>
        
            <AnnoyingAdd
              open={openAdd}
              onClose={() => {
                setOpenAdd(false);
              }}
            />
        </div>
    )
}

