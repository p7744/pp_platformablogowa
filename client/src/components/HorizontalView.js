import * as React from 'react';
import './HorizontalView.css';
import Grid from '@mui/material/Grid';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

export default function HorizontalView( props ) {
return(
    <>
    <Grid className={'test'} sx={{ flexGrow: 1 }} container spacing={2}>
        <Typography className={'proplabel'}  variant="h4" gutterBottom component="div"
                    sx={{ alignItems: 'left' ,
                          marginTop: '20px'

                    }} >{props.title}</Typography>


        <div id="scroller-wrapper">
            <div id="scroller">
                {props.content}
            </div>
        </div>

    </Grid>
    </>
)
}