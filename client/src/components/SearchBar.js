import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import InputBase from '@mui/material/InputBase';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import {useNavigate} from "react-router-dom";

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

export default function SearchBar() {
    // Get the input field
    const navigate = useNavigate()
    var input = document.getElementById("ele");
    console.log(input);

    // Execute a function when the user presses a key on the keyboard
    if(input != null){
        input.addEventListener("keypress", function(event) {
          // If the user presses the "Enter" key on the keyboard
          if (event.key === "Enter") {
            // Cancel the default action, if needed
            event.preventDefault();
            var textHolder = document.getElementById("contentHolder").value;
            if(textHolder!=null){
                console.log(textHolder);
                    if(textHolder.includes("#")){
                        textHolder = textHolder.replace("#","TAG-");
                        console.log(textHolder);
                    }

                    if(textHolder.includes("@")){
                        textHolder = textHolder.replace("@","AUT-");
                        console.log(textHolder);
                    }

                    if (textHolder.includes("$")) {
                        textHolder = textHolder.replace("$", "TIT-")
                    }
                navigate("/search?query="+textHolder);
                window.location.reload();
            }
          }
        });
    }


return(
    <Search id = "ele">
        <SearchIconWrapper>
            <SearchIcon />
        </SearchIconWrapper>
        <StyledInputBase id="contentHolder"
            placeholder="Search…"
            inputProps={{ 'aria-label': 'search' }}
        />
    </Search>
)
}
