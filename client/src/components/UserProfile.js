import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { green } from '@mui/material/colors';
import './UserProfile.css';
import Link from "@mui/material/Link";
import { useNavigate, useParams } from 'react-router-dom'
import {CardActionArea, Hidden} from "@mui/material";
import {makeStyles} from "@mui/styles";
import Button from '@mui/material/Button';
import { useState, useEffect } from 'react';
import Popup from './Popup'
import ArticlePost from './ArticlePost';
import useAuth from './../hooks/useAuth';
import useSnackbar, {SnapbarMessageTypes} from './../hooks/useSnackbar';
import Stack from '@mui/material/Stack';

import ArticleService from './../services/ArticleService';
import UserService from './../services/UserService';
import MediaService from '../services/MediaService';

import ObservationService from '../services/ObservationService';
import ObserverService from './../services/ObserverService';


async function unobserveUser(observing, observed, setSnackbar, observedUser, setObservedUser, setAuth) {

    const observingServ = new ObservationService();
    
    let success = false;

    await observingServ.stopObservingUser(observing?.accessToken, observing?.pk, observed.id)
        .then(r => {
            if (r != null)
                success = true;
        })
        .catch(e => {console.error(e); success = false});

    if (!success)
        setSnackbar(SnapbarMessageTypes.Error, "Could not unobserve this user, try again.")
    else {
        setAuth(prev => {
            return {...prev, observations: [...observing?.observations.filter(v => {
                return v?.observatedUser != observed.id 
            })]}
        })
    }    
    setObservedUser(!observedUser);
}

async function observeUser(observing, observed, setSnackbar, observedUser, setObservedUser, setAuth) {

    const observingServ = new ObservationService();

    let success = false;
    let observation = null;

    await observingServ.observateUser(observing?.accessToken, observed.id)
        .then(r => {
            if (r != null) {
                success = true;
                observation = r;
            }
        })
        .catch(e => {console.error(e); });

    if (!success)
        setSnackbar(SnapbarMessageTypes.Error, "Could not observe this user, try again.")
    else {
        setAuth(prev => {
            return {...prev, observations: [...observing?.observations, observation]};
        })
    }    
    setObservedUser(!observedUser);
}

function addEditButton(loggedUser, pageUser, userObserver, navigate, setSnackbar, observedUser, setObservedUser, setAuth){
    if(loggedUser?.accessToken == null)
        return

    if(loggedUser.usrnm == pageUser.login)
        return

    if(userObserver.some(el => el.id == loggedUser.pk)) {
        return(
            <Button size="small" style={{marginTop: "13px", marginLeft: "20px"}}
            onClick={() => unobserveUser(loggedUser, pageUser, setSnackbar, observedUser, setObservedUser, setAuth)}>Unobserve</Button>
        )
    
    }
        
    return(
        <Button size="small" style={{marginTop: "13px", marginLeft: "20px"}}
        onClick={() => observeUser(loggedUser, pageUser, setSnackbar, observedUser, setObservedUser, setAuth)}>Observe</Button>
    )

}


export default function UserProfilePage () {
    const [visible, setVisibility] = useState(false);


    const [loginU, setLogin] = useState(false)

    const _articleService = new ArticleService();
    const _userService = new UserService();
    const _mediaService = new MediaService();
    const _observationService = new ObservationService();
    const _observerService = new ObserverService();

    const setSnack = useSnackbar();

    const { login } = useParams()

    let [isFollowPopupOpen, setFollowPopupOpen] = useState(false);
    const toggleFollowPopup = () => { setFollowPopupOpen(!isFollowPopupOpen); }

    let [isFollowedPopupOpen, setFollowedPopupOpen] = useState(false);
    const toggleFollowedPopup = () => { setFollowedPopupOpen(!isFollowedPopupOpen); }

    let [isArticlePopupOpen, setArticlePopupOpen] = useState(false);
    const toggleArticlePopup = () => { setArticlePopupOpen(!isArticlePopupOpen); }

    const [isShown, setIsShown] = useState(false);

    const navigate = useNavigate()

    const [data, setData] = useState([])

    const { auth, setAuth } = useAuth()

    const [userData, setUserData] = useState(null)

    const [observedUser, setObservedUser] = useState(false);

    // variable that stores current user profile
    var userProfile = null

    // variable that will store list of profiles that user have followed
    const [userObservation, setUserObservation] = useState([])
//    var userObservation = null
    const [userObservationProfiles, setUserObservationProfiles] = useState([])


    // variable that will store list of profiles that user is following
    const [userObserver, setUserObserver] = useState([])
//    var userObserver = null
    const [userObserverProfiles, setUserObserverProfiles] = useState([])

    const [test, setTest] = useState([])

    var fdTest = ["daniel32", "qwerty", "pololo"]

    var frTest = ["pololo", "qwerty", "daniel32", "asdfgh" ]

    const loadUser = async () =>{
        let userLogin = login.replace(':', '');
        var user = null;

        let data = await _userService.getProfileByLogin(userLogin);
        if (data == null) {
            navigate("/404")
            return;
        }

            
        user = data;
        userProfile = data

        const observations = await _observationService.getObservations(null, null, user.id)

        const observers = await _observerService.getObservers(null, null, user.id);

        userProfile = user;
        setUserData(userProfile)

        var temp1 = []
        
        console.log(observers)

        for (let dataV3 of observations) {
            temp1.push(await _userService.getProfileByLogin(dataV3.observatedUser))
        }

        setUserObservationProfiles([...temp1]);

        var temp = []

        for (let dataV3 of observers) {
            console.log(dataV3);
            temp.push(await _userService.getProfileByLogin(dataV3.observingUser));
        }

        setUserObserverProfiles([...temp]);
    }

    const loadArticles = async () => {
        let articles = null;
        let users = [];

        await _articleService.getArticles(1, 5, userProfile.id)
            .then(data => { articles = data.results })
            .catch(err => console.error(err));

        let articles_to_save = articles;

        let i = 0;
        for (let article of articles) {
            articles_to_save[i]["authorID"] = userProfile.login;
            await _mediaService.getMedias(1,5,article.articleID)
                .then(data => {
                    if (data != null) {
                        articles_to_save[i]["Media"] = `${process.env.REACT_APP_MEDIA_SERVICE_IP}${data.results[0].multimediaPath}`;
                    }
                });
            i++;
        }
        console.log("art");
        setData(articles);
        console.log(articles.length);
    }


    useEffect(() => {
        const funnkcja = async () =>{
            await loadUser()
            await loadArticles()
        }
        funnkcja()
    },[observedUser, login])


    return (
    <div className = "main">
        { userData != null &&
            <div className = "profile-page">
            <div className="profile-top">
                <div className="name-pic-container">
                    <div className="avatar">
                        <Avatar sx={{ bgcolor: red[500],borderRadius: '50%' }} src={`${process.env.REACT_APP_USER_SERVICE_IP}${userData?.profile_picture}`}>
                            {(userData!=null && userData.login) ?  userData.login.toString()[0]  : ""}
                        </Avatar>
                    </div>
                    <div className="name">
                        <div>
                        <p>
                            <strong>{(userData!=null && userData.login) ?  userData.login.toString(): ""}</strong>
                        </p>
                        </div>
                        <div>
                            <p style={{fontSize: "14px"}}>
                            {(userData!=null && userData.description) ?  userData.description.toString(): ""}
                            </p>
                        </div>
                    </div>
                <div className="editButton">
                    {addEditButton(auth, userData, userObserverProfiles, navigate, setSnack, observedUser, setObservedUser, setAuth)}
                </div>
            </div>
                    {console.log("asdsad")}
                    {console.log(userObservationProfiles)}
                    {console.log(userObserverProfiles)}
            <div className="stats">
                <div className="statsEle">
                    <Button variant="contained" size="small" onClick={()=> {setFollowPopupOpen(true);}}>Follows</Button>
                    <p>{userObservationProfiles?userObservationProfiles.length:0}</p>
                    {
                    isFollowPopupOpen &&
                        <Popup content={userObservationProfiles.map(user =>{
                            return (
                                <Stack spacing={2}>
                                    <MiniProfileCard id={user.id} observedUser={observedUser} setObservedUser={setObservedUser}
                                                    login={user?.login} profile_picture={user.profile_picture} setAuth={setAuth}
                                                    desc={user?.description.slice(0, 20)}/>
                                </Stack>
                                
                                // <div className="user">
                                //     <Avatar sx={{ bgcolor: red[500],borderRadius: '50%' }} onClick={ () => {navigate(`/userProfile/${user.login}`); window.location.reload();}}>{user.login[0].toString()}</Avatar>
                                //     <p className="user-name">{user.login}</p>
                                //     <p className="user-desc">{user.description}</p>
                                //     <button onClick={()=> {console.log("dales followa :) typowi o id " + user.id)}}>Observe</button>
                                // </div>
                                );
                            })}
                            handleClose={toggleFollowPopup}
                    />
                    }

                </div>
                <div className="statsEle">
                    <Button variant="contained" size="small" onClick={()=> {setFollowedPopupOpen(true);}}>Followed</Button>
                    <p>{ userObserverProfiles ? userObserverProfiles.length : 0}</p>

                    {isFollowedPopupOpen &&
                        <Popup content={userObserverProfiles.map(user => {
                            return (
                                <Stack spacing={2}>
                                    <MiniProfileCard id={user.id} observedUser={observedUser} setObservedUser={setObservedUser}
                                                    login={user?.login} profile_picture={user.profile_picture} setAuth={setAuth}
                                                    desc={user?.description.slice(0, 20)}/>
                                </Stack>
                                );
                            })}
                            handleClose={toggleFollowedPopup}
                    />}

                </div>
                <div className="statsEle">
                    <Button variant="contained" size="small" onClick={()=> {setArticlePopupOpen(true);}}>Articles count</Button>
                    <p>{ data? data.length:0}</p>

                </div>
            </div>
        </div>
        <div className="articles">
            {data.map(art => {
                return (
                    <div className="articleDiv">
                        <ArticlePost title = {art['title']}  name = {art['authorID']} media = {art['Media']} date = {art['createdAt']} articlePreview = {art['body'].slice(0, 100)} 
                                profilePicture = {userData?.profile_picture} articleId = {art["articleID"]}
                                style={{marginTop: "20px", marginBotton: "20px"}}/>
                    </div>
                )
            })}
        </div>
        </div>
        }
    </div>

    );
}

function MiniProfileCard(props) {
    const navigate = useNavigate();
    const {auth} = useAuth();
    const setSnackbar = useSnackbar();

    const [didChange, setDidChange] = React.useState(false);

    React.useEffect(() => {
        // Just to force updating this component
        console.log("hello")
    }, [didChange]);

    return (
        <Stack direction="row" spacing={12} justifyContent="center">
            <Avatar sx={{ bgcolor: red[500],borderRadius: '50%' }} style={{cursor: 'pointer'}}
                    onClick={ () => {navigate(`/userProfile/${props.login}`);}}
                        src={`${process.env.REACT_APP_USER_SERVICE_IP}${props?.profile_picture}`}
                    >
                    </Avatar>
                    <Stack spacing={0.5} alignItems="flex-start">
                        <Typography><strong>{props.login}</strong></Typography>
                        <Typography>{props.desc}</Typography>
                    </Stack>
            
                    { auth?.accessToken && props.id != auth?.pk ? (
            <Button variant="contained" size="small" style={{maxHeight: "30px"}}
                onClick={(e) => {
                    if (!auth?.observations?.some(el => el.observatedUser == props.id )) {
                        observeUser(auth, props, setSnackbar, props.observedUser, props.setObservedUser, props.setAuth)
                    }
                    else {
                        unobserveUser(auth, props, setSnackbar, props.observedUser, props.setObservedUser, props.setAuth)
                    }
                }}
            >
                 { !auth?.observations?.some(el => el.observatedUser == props.id ) ? "Follow" : "Unfollow" }
            </Button> 
            ) : <Hidden sx={{width: "50px"}}/> }
        </Stack>
            

    ) 
}