import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { green } from '@mui/material/colors';
import './ArticlePost.css';
import Link from "@mui/material/Link";
import { useNavigate } from 'react-router-dom'
import {CardActionArea} from "@mui/material";
import {makeStyles} from "@mui/styles";


function addMedia(props, navigate){

    var image = 'https://i.imgur.com/Y7O5tuh.png'

    if(props.media !== '')
        image = props.media
    
    return(

        <div className= "imagediv">
           { props.media != null ? (
            <CardActionArea onClick={ () => {navigate("/a/" + props.articleId)}}>
            <CardMedia
            component="img"
            image={image}
            alt="article image"
            />
            </CardActionArea>
            ) : "" }
        </div>
    )
}

function addPFP(props, navigate){

    if(props.profilePicture != null)
        return(
            <CardMedia
                sx ={{
                    maxWidth: '100%',
                    maxHeight: '100%'
                }}
                component="img"
                image={`${process.env.REACT_APP_USER_SERVICE_IP}` + props.profilePicture}
                alt="article image"
                onClick={() => navigate(`/userProfile/${props.name}`)}
            />
        )
    else return(
        <>
        {props.name.toString()[0]}
        </>
        )
}
export default function ArticlePost(props) {

    const navigate = useNavigate()
    var borderpresent = props.media != null?1:0;
    const classes = makeStyles({
        edit: {  ".MuiCardActionArea-root &:hover": { backgroundColor: "palegreen",opacity: 1 } } ,
        greenButton: {
            backgroundColor: green[500],
            color: '#FFF',
            '&:hover': {
                backgroundColor: green[400],
                color: '#FFF',
                opacity: 12
            }
        }
    })();

    return (
        <Card calssName="MainCard" sx={{
            borderRadius: '2%',
            border: 1,
            alignment: 'left',
            display: 'inline-block',
            marginRight: '10px'

        }}>
            <CardHeader
                sx = {{borderBottom: borderpresent}}
                avatar={
                   <CardActionArea  sx={{borderRadius: '50%'} }
                        className = {classes.edit}
                                     aria-label="edit"
                         onClick={ () => {navigate(`/userProfile/${props.name}`)}}>
                    <Avatar className="avatar" sx={{ bgcolor: red[500],borderRadius: '50%' }}  >
                        {addPFP(props, navigate)}
                    </Avatar>
                    </CardActionArea>


                }
                title ={
                    <Link sx = {{color : 'black'}} className = "profileLink" href="#" underline="hover" onClick={ () => {navigate(`/userProfile/${props.name}`)} }>
                        {props.name ?? ""}
                    </Link>}

                subheader={
                    <Link sx = {{color : 'gray'}} className = "profileLink" href="#" underline="hover" onClick={ () => {navigate(`/userProfile/${props.name}`)} }>
                        {new Date(props.date).toDateString() ?? ""}
                    </Link>}
            />

            {addMedia(props, navigate)}

            <CardActionArea onClick={ () => {navigate("/a/" + props.articleId)}}>
            <CardContent sx = {{borderTop:1}}>
                <Typography className="test" variant="body2" color="text.secondary">
                    <b className="articleTitle">{props.title}</b> <br/>
                    {props.articlePreview}...
                </Typography>
            </CardContent>
            </CardActionArea>

        </Card>
    );
}
