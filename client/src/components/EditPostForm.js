import React, {Component} from 'react';
import './formstyle.css';
import ReactMarkdown from 'react-markdown'
import ReactDOM from 'react-dom';
import ArticleService from '../services/ArticleService';
import remarkGfm from 'remark-gfm';
import remarkMath from 'remark-math';
import rehypeKatex from 'rehype-katex';
import rehypeRaw from 'rehype-raw';
import rehypeHighlight from 'rehype-highlight'
import rehypeSanitize from 'rehype-sanitize';
import 'katex/dist/katex.min.css';
import useAuth from '../hooks/useAuth';
import useSnackbar, {SnapbarMessageTypes} from '../hooks/useSnackbar';
import {Navigate, useNavigate, useParams} from 'react-router-dom';
import MediaService from '../services/MediaService';
import "github-markdown-css/github-markdown-light.css";
import {Modal, Box, IconButton, Button, CircularProgress} from '@mui/material';
import {FormatItalic, FormatBold, AddPhotoAlternate, Title} from "@mui/icons-material";
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter'
import {a11yDark as vs} from 'react-syntax-highlighter/dist/esm/styles/prism'
import mediaService from "../services/MediaService";
import useQuery from "../hooks/useQuery";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    height: '85%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    borderRadius: '2%',
    boxShadow: 24,
    p: 2,
    overflow: 'scroll',
    overflowX: 'hidden',
    display: 'block'
};

function EditPostForm() {

    const {articleId} = useParams();

    const {auth} = useAuth();
    const query = useQuery();

    const setSnackbar = useSnackbar();
    const navigate = useNavigate();

    const [convertedArticle, setconvertedArticle] = React.useState("");
    const articlesService = new ArticleService();
    const [article, setArticle] = React.useState({});
    const [medias, setMedias] = React.useState([]);


    const [previewOpen, setPreviewOpen] = React.useState(false);
    const [title, setTitle] = React.useState("");

    var sometext = "";

    const preview = (e) => {
        console.log(e.target.files)
        if (e.target.files.length > 0) {
            var src = URL.createObjectURL(e.target.files[0]);
            document.getElementById("zdjeciepreview").src = src;
        }

    };


    function helper(type) {
        var myField = document.getElementById("tresc");
        var myValue = myField.value.slice(myField.selectionStart, myField.selectionEnd);

        if (type == 'bold')
            myValue = "**" + myValue + "**";
        else if (type == "italic")
            myValue = "*" + myValue + "*";
        else if (type == "header1")
            myValue = "\n# " + myValue;
        else if (type == "header2")
            myValue = "\n## " + myValue;
        else if (type == "header3")
            myValue = "\n### " + myValue;
        else if (type == "image")
            myValue = '![ ALTERNATIVE TEXT](HERE INSERT LINK TO YOUR IMAGE "TEXT FOR MOUSEOVER") ';
        //IE support
        if (document.selection) {
            myField.focus();
            var sel = document.selection.createRange();
            sel.text = myValue;
        }
        //MOZILLA and others
        else if (myField.selectionStart || myField.selectionStart == '0') {
            var startPos = myField.selectionStart;
            var endPos = myField.selectionEnd;
            myField.value = myField.value.substring(0, startPos)
                + myValue
                + myField.value.substring(endPos, myField.value.length);
        } else {
            myField.value += myValue;
        }
    }


    const uploadpost = async () => {

        console.log(document.getElementById("tytol").value);

        try {
            const resp = await articlesService.modifyArticle(auth?.accessToken, articleId,
                document.getElementById("tytol").value,
                document.getElementById("tresc").value,
                document.getElementById("tagi").value,
                true
            );

            if (resp != null)
                setSnackbar(SnapbarMessageTypes.Error, "Could not save article, try again with title, category and body inputed.")
            else {
                setSnackbar(SnapbarMessageTypes.Success, "Edited an article successfuly")
                let redirect = query.get("redirect-to")
                if (redirect != null)
                    navigate(redirect)
                else
                    navigate("/")
            }
        } catch (err) {
            console.error(err);
        }


    };


    const markdownpreview = () => {


        sometext = document.getElementById("tresc").value;
        setconvertedArticle(sometext)
        // document.getElementById("tytolslot").innerHTML = '<h1 class="tytol">' + document.getElementById("tytol").value + "</h1></br></br>";

        setPreviewOpen(true);

    };


    const uploadimage = async () => {
        const mediaService = new MediaService();

        var z1 = document.getElementById("zdjecie");
        if (z1.files.length > 0) {
            var src = URL.createObjectURL(z1.files[0]);

            let media = null;

            try {
                media = await mediaService.addNewMedia(auth?.accessToken, z1.files[0], "img", article?.articleID)

                if (!media?.multimediaPath) {
                    setSnackbar(SnapbarMessageTypes.Error, "Could not upload image");
                    return;
                }
                console.log(media);
                document.getElementById("zdjeciepreview").src = "https://www.narda-sts.com/fileadmin/_processed_/b/9/csm_no-image-available_EN_d88b60dc52.png";
                document.getElementById("ilinklist").innerHTML += "<li>" + `${process.env.REACT_APP_MEDIA_SERVICE_IP}` + `${media?.multimediaPath}` + "</li>";


            } catch (err) {
                setSnackbar(SnapbarMessageTypes.Error, "Could not upload image");
                return;
            }


            z1.value = '';
            setSnackbar(SnapbarMessageTypes.Success, "Uploaded new image");


        }
    };


    React.useEffect(() => {
        const fetchData = async () => {
            let artRes = null;
            try {
                artRes = await articlesService.getArticleById(articleId)

                if (artRes?.authorID != auth?.pk)
                    navigate("/a/" + articleId);

                console.log(artRes)
                setArticle(artRes);


                console.log(article)
                const body = artRes?.body;

                document.getElementById("tytol").value = artRes?.title;
                document.getElementById("tagi").value = artRes?.category;
                document.getElementById("tresc").value = body;
            } catch (err) {
                console.error(err);
            }





            const mediaService = new MediaService();

            try {
                let mediaHtml = "";
                const mediaRes = await mediaService.getMedias(null, null, articleId);
                console.log(mediaRes)
                if (Array.isArray(mediaRes)) {
                    mediaRes.forEach(m => {
                        mediaHtml += "<li>" + `${process.env.REACT_APP_MEDIA_SERVICE_IP}${m?.multimediaPath}` + "</li>";
                    });
                    document.getElementById("ilinklist").innerHTML = mediaHtml;
                }
            } catch (err) {
                console.error(err)
                //window.reload();
            }

        };
        fetchData();
    }, []);

    return (
        <>
            <div className="leftpanel">
            </div>
            <div className="center">
                {article?.articleID != null ? (
                    <div className="formularz">
                        <h4>Input your title:</h4>
                        <textarea id="tytol" onChange={(e) => setTitle(e.target.value)}></textarea>
                        <h4>Input your category:</h4>
                        <textarea id="tagi"></textarea>
                        <h4>Input your article in markdown format:</h4>

                        <IconButton onClick={() => {
                            helper('bold')
                        }} className="markdownfunc"><FormatBold/></IconButton>
                        <IconButton onClick={() => {
                            helper('italic')
                        }} className="markdownfunc"><FormatItalic/></IconButton>
                        <IconButton onClick={() => {
                            helper('header1')
                        }} className="markdownfunc"><Title/> 1</IconButton>
                        <IconButton onClick={() => {
                            helper('header2')
                        }} className="markdownfunc"><Title/> 2</IconButton>
                        <IconButton onClick={() => {
                            helper('header3')
                        }} className="markdownfunc"><Title/> 3</IconButton>
                        <IconButton onClick={() => {
                            helper('image')
                        }} className="markdownfunc"><AddPhotoAlternate/></IconButton>


                        <textarea id="tresc" ></textarea>
                        <Button variant="contained" size="small" onClick={markdownpreview}
                                className="middlebutton">Preview</Button>

                        {/* <div className="markdown-body" id="markdownoutput"> <ReactMarkdown children={convertedArticle}
                                        remarkPlugins={[remarkGfm, remarkMath]}
                                        rehypePlugins={[rehypeKatex, rehypeRaw, rehypeSanitize]}>
                                </ReactMarkdown>  </div> */}

                        <Button variant="contained" size="small" onClick={uploadpost}
                                className="middlebutton">Save Changes</Button>

                    </div>
                ) : (
                    <CircularProgress/>
                )}


            </div>
            <div className="rightpanel">
                <label for="zdjecie" className="inputsize, labelinput" >Select Image
                    <img id="zdjeciepreview"
                         src="https://www.narda-sts.com/fileadmin/_processed_/b/9/csm_no-image-available_EN_d88b60dc52.png"
                         alt="your image"/>
                </label>

                <input type="file" className="inputsize" id="zdjecie" accept="image/png, image/jpeg, image/gif"
                       onChange={preview}></input>

                <Button variant="contained" size="small" onClick={uploadimage} className="rightbutton">upload</Button>
                <h4>Your uploaded images:</h4>
                <div className="imagelink" id="ilink" style={{overflowWrap: "anywhere"}}>
                    <ol id="ilinklist">
                    </ol>


                </div>
            </div>
            <Modal
                open={previewOpen}
                onClose={() => {
                    setPreviewOpen(false);
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <div className="markdown-body" id="markdownoutput"><ReactMarkdown
                        remarkPlugins={[remarkGfm, remarkMath]}
                        rehypePlugins={[rehypeKatex, rehypeRaw]}
                        components={{
                            code({node, inline, className, children, ...props}) {
                                const match = /language-(\w+)/.exec(className || '')
                                return !inline && match ? (
                                    <SyntaxHighlighter
                                        children={String(children).replace(/\n$/, '')}
                                        style={vs}
                                        language={match[1]}
                                        PreTag="div"
                                        {...props}
                                    />
                                ) : (
                                    <code className={className} {...props}>
                                        {children}
                                    </code>
                                )
                            }
                        }}
                    >
                        {convertedArticle}
                    </ReactMarkdown></div>
                </Box>
            </Modal>
        </>


    )

}

export default EditPostForm;
