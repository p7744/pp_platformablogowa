import styled from "styled-components";
import * as React from 'react';
import * as Colors from "./ColorPallete";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Link } from "@mui/material";
import { colors } from "@mui/material";
import {useNavigate, useParams} from "react-router-dom";
import useAuth from '../hooks/useAuth';
import UserService from "../services/UserService";
import {SnapbarMessageTypes} from "../App";
import useSnackbar from "../hooks/useSnackbar";

const ActivateViewDiv = styled.div`
    display: flex;
    justify-content: center;
`;

const ActivateForm = styled.div`
    outline: solid 1px ${Colors.BORDER_COLOR};
    position: absolute;
    top: 25%;
    left: 36%;
    height: 45%;
    padding: 30px;
    width: 400px;
    background-color: ${Colors.PRIMARY_COLOR};
`;

const ErrorMessage = styled.p`
    color: #FF0000;
`;

export default function ActivateUser(props) {

    const [success, setSucces] = React.useState(false);

    const navigate = useNavigate();
    const { email } = useParams();

    const [errorOccured, setErrorOccured] = React.useState(false);
    const [errorMessage, setErrorMessage] = React.useState("");

    const [token, setToken] = React.useState("");

    const setSnapbar = useSnackbar();

    React.useEffect(() => {
        setErrorOccured(false)
    }, [token]);

    const activateUser = async (e) => {
        e.preventDefault();
        console.log(email)
        const userService = new UserService();
        await userService.activateUser(email, token)
            .then(r => {
                console.log(r);
                setSnapbar(SnapbarMessageTypes.Success, "Activated your user succesfully, now you can sign in")
                navigate("/login");
            })
            .catch(e => {
                console.error(e)
                if (String(e).toLowerCase().includes("user already active")) {
                    setErrorMessage("Could not activate user, user is already active");
                } else {
                    setErrorMessage("Could not activate user, check if your token is correct");
                }

                setErrorOccured(true);
            })
    }

    return (
        <ActivateViewDiv className="ActivateUser">
            <ActivateForm>
                <h1 style={{fontSize: "40px"}}>Activate your account</h1>
                <br/><br/>
                <TextField id="filled-basic" label="Activate user token"
                           variant="filled" sx={{width: '80%'}} value={token}
                           onChange={(e => { setToken(e.target.value) })}/>
                <br/><br/>
                <br></br><br></br>
                <Button variant="contained" sx={{width: '80%'}} onClick={activateUser}>Activate</Button>
                { errorOccured ? <ErrorMessage>{errorMessage}</ErrorMessage> : ""}

            </ActivateForm>
        </ActivateViewDiv>
    )
}
