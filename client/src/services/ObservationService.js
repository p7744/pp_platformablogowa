

const USER_SERVICE_URL = process.env.REACT_APP_USER_SERVICE_IP;

class ObservationService {
    
    constructor() { }

    async getObservations(page, per_page, userId) {

        if ((!Number.isInteger(page) && page != null) || (!Number.isInteger(per_page) && per_page != null) )
            throw new Error("Page and per_page needs to be integers or null");

        let queries = "?";

        if (page != null)
            queries += `page=${page}&`;
        if (per_page != null && page != null)
            queries += `per_page=${per_page}&`;

        if (Number.isInteger(userId))
            queries += `user_id=${userId}`;
        
        let profileResult = null;
        let success = true;

        await fetch(`${USER_SERVICE_URL}/api/v1/observations${queries}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(response => {
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            profileResult = data;
            success = true
        })
        .catch(error => {
            success = false;
        })
        return success ? profileResult : null;
    }

    async observateUser(accessToken, userToObserveId) {
        
        let profileResult = null;
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/observations`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                observatedUser: userToObserveId
            })
        })
        .then(response => {
            status = response.status;
            if (response.status != 201)
                success = false;
            return response.json()
        })
        .then(data => {
            profileResult = data;
            console.log(profileResult)
            success = true
        })
        .catch(error => {
            success = false;
        })

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");
        
        

        return success ? profileResult : null;
    }

    async stopObservingUser(accessToken, userId1, userId2) {
        
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/observations/${userId1}&${userId2}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 204)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
        })
        .catch(error => {
            success = false;
        })

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success;
    }

    async getObservationByIds(id1, id2) {
        if (!Number.isInteger(id1) || !Number.isInteger(id2))
            throw new Error("id1 and 2 must be an int");
        
        let profileResult = null;
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/observations/${id1}&${id2}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 200)
                success = false;
            return response.json()
        })
        .then(data => {
            profileResult = data;
            success = true
        })
        .catch(error => {
            success = false;
        })

        return success ? profileResult : null;
    } 
}

export default ObservationService;