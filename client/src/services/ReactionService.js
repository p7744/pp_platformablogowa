const ARTICLE_SERVICE_URL = process.env.REACT_APP_ARTICLE_SERVICE_IP;

class ReactionService {
    
    constructor() { }

    async addReaction(accessToken, articleId, reaction) {

        let success = true;
        let result = null;
        let status = 200;


        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/reactions`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                article: articleId,
                liked: reaction
            })
        })
        .then(response => {
            status = response.status;
            if (response.status !== 201)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async getReactions(page, per_page, authorId, articleId) {
        let queries = "?";

        if (Number.isInteger(page))
            queries += `page=${page}&`;
        if (Number.isInteger(per_page) && page != null)
            queries += `per_page=${per_page}&`

        if (Number.isInteger(authorId))
            queries += `author_id=${authorId}&`

        if (Number.isInteger(articleId))
            queries += `article_id=${articleId}`
        
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/reactions${queries}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        return success ? result : null;
    }

    async getReactionById(reactionId) {
        
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/reactions/${reactionId}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        return success ? result : null;
    }

    async deleteReaction(accessToken, reactionId) {

        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/reactions/${reactionId}`, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            },
        })
        .then(response => {
            status = response.status;
            if (response.status !== 204)
                success = false;
            return response.json()
        })
        .then(data => {
            result = data;
        })
        .catch(error => {
            success = false;
        });
        console.error(result)
        return success;
    }
}

export default ReactionService;