const USER_SERVICE_URL = process.env.REACT_APP_USER_SERVICE_IP;

class SubscriptionService {

    constructor() { }

    static SUBSCRIPTION_PERIODS = {
        oneMonth: 1,
        threeMonths: 3,
        oneYear: 12
    }

    async makePayments(accessToken, subId, amount) {

        let success = true;
        let status = 200;
        let response = null;

        await fetch(`${USER_SERVICE_URL}/api/v1/payments`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                subscription: subId,
                payedAmount: amount
            })
        })
        .then(response => {
            status = response.status;
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            response = data
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

            console.log(response)
        return success;
    }

    async getSubscriptions(accessToken) {
        if (typeof accessToken != "string")
            throw new Error("token must be string");

        let success = true;
        let status = 200;
        let result = null;

        await fetch(`${USER_SERVICE_URL}/api/v1/subscriptions`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
        })
        .then(response => {
            status = response.status;
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            result = data;
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async subscribeUser(accessToken, period) {
        
        
        if (!(Object.values(SubscriptionService.SUBSCRIPTION_PERIODS).includes(period)))
            throw new Error("period must be of enum SUBCRIPTION_PERIODS type");

        let success = true;
        let status = 200;
        let result = null;

        await fetch(`${USER_SERVICE_URL}/api/v1/subscriptions`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                "period": period
            })
        })
        .then(response => {
            status = response.status;
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            result = data;
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");
        
        console.log(result)
        
        return success ? result : null;
    }

    async getSubscriptionById(accessToken, id) {
        if (typeof accessToken != "string" || !Number.isInteger(id))
            throw new Error("token must be string and id an int");

        let success = true;
        let status = 200;
        let result = null;

        await fetch(`${USER_SERVICE_URL}/api/v1/subscriptions/${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
        })
        .then(response => {
            status = response.status;
            if (response >= 400)
                success = false;
            return response.json()
        })
        .then(data => {
            result = data;
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async resignSubcriptionWithId(accessToken, id) {
        if (typeof accessToken != "string" || !Number.isInteger(id))
            throw new Error("token must be string and id an int");

        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/subscriptions/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
        })
        .then(response => {
            status = response.status;
            console.log(response.status)
            if (response.status !== 204) {
                console.log("why")
                success = false;
            }
            return response.json()
        })
        .then(data => {
            success = true
        })
        .catch(error => {
            
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");
        console.log(success)
        return success;
    }
}

export default SubscriptionService;