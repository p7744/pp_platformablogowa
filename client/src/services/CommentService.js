
const ARTICLE_SERVICE_URL = process.env.REACT_APP_ARTICLE_SERVICE_IP;

class CommentService {
    
    constructor() { }

    async addComment(accessToken, articleId, comment) {

        let success = true;
        let result = null;
        let status = 200;


        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/comments`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                article: articleId,
                description: comment
            })
        })
        .then(response => {
            status = response.status;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status === 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");
        if (status !== 201)
            throw new Error(JSON.stringify(result));

        return result;
    }

    async getComments(page, per_page, authorId, articleId) {
        let queries = "?";

        if (Number.isInteger(page))
            queries += `page=${page}&`;
        if (Number.isInteger(per_page) && page != null)
            queries += `per_page=${per_page}&`

        if (authorId != null)
            queries += `author_id=${authorId}&`

        if (articleId != null)
            queries += `article_id=${articleId}`
        
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/comments${queries}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        return success ? result : null;
    }

    async getCommentById(commentId) {
        if (!Number.isInteger(commentId))
            throw new Error("comment id must a string");
        
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/comments/${commentId}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        

        return success ? result : null;
    }

    async modifyComment(accessToken, commentId, newComment) {
        if (!typeof accessToken == "string" || !Number.isInteger(commentId) || !typeof newComment == "string")
            throw new Error("commentId must be integer and token and new comment must be string");

        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/comments/${commentId}`, {
            method: "PUT",
            headers: {
                "Content-type": "application/json",
            },
            body: {
                description: newComment
            }
        })
        .then(response => {
            status = response.status;
            if (response.status != 204)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success;
    }

    async deleteComment(accessToken, commentId) {
        if (!typeof accessToken == "string" || !Number.isInteger(commentId))
            throw new Error("commentId must be integer and token must be string");

        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/comments/${commentId}`, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization": "Bearer " + accessToken
            },
        })
        .then(response => {
            status = response.status;
            if (response.status !== 204)
                success = false;
            return response.json()
        })
        .then(data => {
            result =data;
            success = true
        })
        .catch(error => {
            //console.error(error);
            if (result != null)
                success = false;
        });

        console.log(success)

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");
            
        return success;
    }
}

export default CommentService;
