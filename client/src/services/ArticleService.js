const ARTICLE_SERVICE_URL = process.env.REACT_APP_ARTICLE_SERVICE_IP;

class ArticleService {
    
    constructor() { }

    async addArticle(accessToken, title, body, category) {
        if (typeof accessToken != "string" || typeof title != "string" || typeof body != "string" || typeof category != "string")
            throw new Error("all parametaers must be strings");

        let success = true;
        let result = null;
        let status = 200;

        const form = new FormData();
        form.append('title', title);
        form.append('body', body);
        form.append('category', category);

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/articles`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
            body: form
        })
        .then(response => {
            status = response.status;
            if (response.status != 202)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async getArticles(page, per_page, authorId, category) {
        let queries = "?";

        if (Number.isInteger(page))
            queries += `page=${page}&`;
        if (Number.isInteger(per_page) && page != null)
            queries += `per_page=${per_page}&`

        if (Number.isInteger(authorId))
            queries += `author_id=${authorId}&`
        
        if (typeof category == "string")
            queries += `category=${category}`

        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/articles${queries}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status !== 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        return success ? result : null;
    }

    async getArticleById(articleId) {
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/articles/${articleId}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
            },
        })
        .then(response => {
            status = response.status;
            if (response.status !== 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        return success ? result : null;
    }

    async modifyArticle(accessToken, articleId, title, body, category, isActive) {
        if (typeof accessToken != "string" || typeof title != "string" || typeof body != "string" || typeof category != "string")
            throw new Error("all parametaers must be strings");

        let success = true;
        let result = null;
        let status = 200;

        console.log(title)
        console.log(body)
        console.log(category)

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/articles/${articleId}`, {
            method: "PUT",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
            body: JSON.stringify({
                title: title,
                body: body,
                category: category,
                isActive: isActive
            })
        })
        .then(response => {
            status = response.status;
            if (response.status !== 200)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        console.error(result);

        return success ? result : null;
    }

    async deleteArticle(accessToken, articleId) {
        let success = true;
        let result = null;
        let status = 200;

        await fetch(`${ARTICLE_SERVICE_URL}/api/v1/articles/${articleId}`, {
            method: "DELETE",
            headers: {
                "Content-type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            }
        })
        .then(response => {
            status = response.status;
            if (response.status != 204)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success;
    }
}

export default ArticleService;