
const MEDIA_SERVICE_URL = process.env.REACT_APP_MEDIA_SERVICE_IP;

class MediaService {
    
    constructor() { }

    async addNewMedia(accessToken, media, mediaType, articleId) {

        let success = true;
        let result = null;
        let status = 200;

        const formData = new FormData();
        formData.append("multimediaPath", media, media.name);
        formData.append("multimediaType", mediaType);
        formData.append("articleID", articleId);

        await fetch(`${MEDIA_SERVICE_URL}/api/v1/media`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
            body: formData
        })
        .then(response => {
            status = response.status;
            if (response.status != 202)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async getMedias(page, per_page, articleId) {

        let queries = "?";

        if (page != null)
            queries += `page=${page}&`;
        if (per_page != null && page != null)
            queries += `per_page=${per_page}&`;

        if (articleId != null)
            queries += `article_id=${articleId}`;
        
        let result = null;
        let status = 200;
        let success = true;

        await fetch(`${MEDIA_SERVICE_URL}/api/v1/media${queries}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(response => {
            status = response.status;
            if (response.status != 202)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
            result = data;
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async deleteMedia(accessToken, mediaId) {
        if (typeof accessToken != "string" ||  !Number.isInteger(mediaId))
            throw new Error("mediaId must be integer and token must be string");

        let success = true;
        let status = 200;

        await fetch(`${MEDIA_SERVICE_URL}/api/v1/media`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            },
        })
        .then(response => {
            status = response.status;
            if (response.status != 202)
                success = false;
            return response.json()
        })
        .then(data => {
            success = true
        })
        .catch(error => {
            success = false;
        });

        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success;
    }
}

export default MediaService;