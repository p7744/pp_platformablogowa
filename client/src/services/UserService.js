

const USER_SERVICE_URL = process.env.REACT_APP_USER_SERVICE_IP;

/**
 * User Service class *
 * Used to connect to users api
 *
 * - Example of usage:
 *      - let service = new UserService();
 *      - await service.getProfileByLogin(login).then(result => { do something });
 */
class UserService {
    constructor() {
    }

    async getProfileByLogin(login) {
        

        let profileResult = null;
        let success = true;

        await fetch(`${USER_SERVICE_URL}/api/v1/profiles/${login}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(response => {
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            profileResult = data;
            success = true
        })
        .catch(error => {
            success = false;
        });

        return success ? profileResult : null;
    }

    async getProfiles(page, per_page) {
        if ((!Number.isInteger(page) && page != null) || (!Number.isInteger(per_page) && per_page != null) )
            throw new Error("Page and per_page needs to be integers or null");

        let queries = "";

        if (page != null)
            queries += `?page=${page}`;
        if (per_page != null && page != null)
            queries += `&per_page=${per_page}`;

        let profileResult = null;
        let success = true;

        await fetch(`${USER_SERVICE_URL}/api/v1/profiles${queries}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
        .then(response => {
            if (!response.ok)
                success = false;
            return response.json()
        })
        .then(data => {
            profileResult = data;
            console.log(data);
            success = true
        })
        .catch(error => {
            success = false;
        })
        return success ? profileResult : null;
    }

    async getUser(accessToken) {
        if (!(typeof accessToken == "string"))
            throw new Error("accessToken must be a String");

        let result = null;
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/users`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            }
        })
        .then(response => {
            status = response.status;
            return response.json()
        })
        .then(data => {
            result = data;
            success = true;
        })
        .catch(error => {
            success = false;
        })
        if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success ? result : null;
    }

    async addUser(email, login, password, birthday) {
        let result = null
        let success = false
        let status = 200
        let error = false;
        let errorMsg = null;

        const form = new FormData();
        form.append('email', email);
        form.append('login', login);
        form.append('password', password);
        form.append('birthday', birthday);

        await fetch(`${USER_SERVICE_URL}/api/v1/users`, {
            method: "POST",
            body: form
        })
            .then(r => {
                status = r.status;
                return r.json();
            })
            .then(d => {
                result = d
            })
            .catch(e => {
                error =true;
                errorMsg = e;
            })

        if (error) {
            throw errorMsg;
        }

        if (status !== 201) {
            throw new Error(JSON.stringify(result));
        }

        return result;
    }

    async modifyUser(accessToken, profilePicture, password, description) {

        const formData = new FormData();
        if (profilePicture != null) {
            formData.append("profile_picture", profilePicture, profilePicture.name);
        }
        if (password != null && password != "") {
            formData.append("password", password);
        }
        formData.append("description", description);

        let result = null;
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/users`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            },
            body: formData,
        })
        .then(response => {
            status = response.status;
            return response.json();
        })
        .then(data => {
            result = data;
            success = true;
        })
        .catch(error => {
            success = false
        })

        console.log(result);

        if (status == 403) {
            
            throw new Error("Credentials were not accepted, incorrect token or expired");
        }
            

        return success;
    }
    async activateUser(email, activateToken) {
        let result = null;
        let success = true;
        let status = 200;
        let error = false;
        console.log(email)

        const form = new FormData();
        form.append('token', activateToken);

        await fetch(`${USER_SERVICE_URL}/api/v1/users/activate/${email}`, {
            method: "PUT",
            body: form
        })
            .then(r => {
                status = r.status;
                return r.json();
            })
            .then(d => {
                result = d;
            })
            .catch(e => {
                error = true;
                result = e;
            })

        if (error)
            throw new Error(result);
        if (status !== 202)
            throw new Error(JSON.stringify(result))

        return result;
    }
    async deleteUser(accessToken) {
        if (!(typeof accessToken == "string"))
            throw new Error("accessToken must be a String");

        let result = null;
        let success = true;
        let status = 200;

        await fetch(`${USER_SERVICE_URL}/api/v1/users`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${accessToken}`
            }
        })
        .then(response => {
            status = response.status;
            return response.json()
        })
        .then(data => {
            success = true;
        })
        .catch(error => {
            success = false;

        })
        if (status !== 204)
            success = false;
        else if (status == 403)
            throw new Error("Credentials were not accepted, incorrect token or expired");

        return success;
    }

    async verifyLogedinUser(token) {
        if (typeof(token) === typeof(string))
            throw new Error("Type of token is not string")

        const verifyTokenData = {"token": `${token}`}
        let verifiedToken = false

        await fetch(`${USER_SERVICE_URL}/api/v1/auth/verify`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(verifyTokenData),
        })
        .then(response => {
        //console.log(response.status)
        if (response.status === 200) {
            verifiedToken = true
            //console.log(verifiedToken)
        }
        return response.json()
        }).catch(error => {
        //verifiedToken = false
        })

        //console.log(verifiedToken)
        return verifiedToken
    }

    async logUserIn(email, password) {
        const loginData = {"email": email, "password": password}
        let success = false
        let result = null
        let error = null;

        await fetch(`${USER_SERVICE_URL}/api/v1/auth` ,{
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginData),
        })
        .then(response => response.json())
        .then(data => {
            result = data;
            if (data.hasOwnProperty('access')) {
                success = true;
            }
            //console.log(data)
        })
        .catch(error => {
            success = false
            error = error;
        })

        if (!success) {
            throw new Error(JSON.stringify(result));
        }

        if (error)
            throw error;

        return success ? result : null;
    }

    async refreshToken(token) {
        if (typeof token != "string")
            throw new Error("Type of token is not string")

        const refreshTokenData = {"refresh": `${token}`}
        var accessToken = null
        var verifiedToken = false
        var result = null

        await fetch(`${USER_SERVICE_URL}/api/v1/auth/refresh`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(refreshTokenData),
        })
        .then(response => {
            if (response.status === 200) {
                verifiedToken = true
            }
            return response.json()
        })
        .then(data => {
            if (verifiedToken) {
                result = data
                //console.log(accessToken)
            }
        })

        if (!verifiedToken)
            throw new Error("Could not be able to refresh");
        
        return verifiedToken ? result : null;
    }
}

export default UserService;